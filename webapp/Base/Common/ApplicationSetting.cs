﻿namespace Base.Common
{
    public class ApplicationSetting
    {
        public string WebAppId { get; set; }

        public string WebAppPassWord { get; set; }

        public string EmailUrl { get; set; }
    }
}