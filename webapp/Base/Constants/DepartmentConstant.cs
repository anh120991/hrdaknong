﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Constants
{
    public class DepartmentConstant
    {
        public const string GetBTDKP = "GetBTDKP";
        public const string GetSubDepartment = "GetSubDepartment";
        public const string GetTotalEmTreatment = "GetTotalEmTreatment";
        public const string GetTotalEmPatientSubject = "GetTotalEmPatientSubject";
        public const string GetTotalPharmaSubDepartment = "GetTotalPharmaSubDepartment";
        public const string GetTotalSubDepartmentMajors = "GetTotalSubDepartmentMajors";
        public const string GetDepartmentMajors = "GetDepartmentMajors";
        public const string GetTotalSubDepartment = "GetTotalSubDepartment";
        
    }
}
