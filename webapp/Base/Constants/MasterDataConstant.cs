﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Constants
{
    public class MasterDataConstant
    {
        public const string GetDonVi = "GetDonVi";
        public const string GetChucVu = "GetChucVu";
        public const string GetTrangThaiHS = "GetTrangThaiHS";
        public const string GetHS = "GetHS";
    }
}
