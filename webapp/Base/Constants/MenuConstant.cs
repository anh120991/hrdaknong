﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Constants
{
    public class MenuConstant
    {
        public const string GetMenu = "GetMenu";
        public const string CreateMenu = "CreateMenu";
        public const string UpdateMenu = "UpdateMenu";
        public const string GetMenuDetail = "GetMenuDetail";
    }
}
