﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Constants
{
    public class PermissionConstant
    {
        public const string GetAllPermission = "GetAllPermission";
        public const string AddNewPermission = "AddNewPermission";
        public const string UpdatePermission = "UpdatePermission";
        public const string GetDetailPermission = "GetDetailPermission";

    }
}
