﻿namespace Base.Constants
{
    public class PermissionKeyConstant
    {
        public const string IsViewMenuUser = "US_01";
        public const string IsViewMenuPermission = "PR_01";
        public const string IsViewMenuRole = "RL_01";
        public const string IsViewMenuMedicalExamination = "ME_01";
        public const string IsViewMenuOutpatient = "OP_01";
        public const string IsViewMenuEmergency = "EM_01";
        public const string IsViewMenuBoarding = "BD_01";
        public const string IsViewMenuReport = "RP_01";
        public const string IsViewMenu = "MN_01";
    }
}