﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Constants
{
    public class ReportConstant
    {

        public const string Thongkedangvien = "Thongkedangvien";
        public const string Dotuoi = "Dotuoi";
        public const string Thongkenhansu = "Thongkenhansu";
        public const string ThongkenhansuDonvi = "ThongkenhansuDonvi";
        public const string Trinhdotinhoc = "Trinhdotinhoc";
        public const string Tilekhenthuong = "Tilekhenthuong";
        public const string Tilekyluat = "Tilekyluat";
        public const string Trinhdohocvan = "Trinhdohocvan";
        public const string Trinhdongoaingu = "Trinhdongoaingu";
        public const string Thongkecbns = "Thongkecbns";
        public const string Donviktkl = "Donviktkl";
        public const string DSktkl = "DSktkl";
    }
}
