﻿namespace Base.Constants
{
    public class TreatmentApiCode
    {
        public const string GetTotalIpPatients = "GetTotalIpPatients";
        public const string GetTotalIpAdmitPatients = "GetTotalIpAdmitPatients";
        public const string GetTotalIpOutDept = "GetTotalIpOutDept";
        public const string GetTotalIpTreatment = "GetTotalIpTreatment";
        public const string GetTotalIpResult = "GetTotalIpResult";
        public const string GetTotalIpPatientSubject = "GetTotalIpPatientSubject";
    }
}