﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Constants
{
    public class UserConstant
    {
        public const string CreateNewUser = "CreateNewUser";
        public const string GetAllUser = "GetAllUser";
        public const string GetDetailUser = "GetDetailUser";
        public const string UpdateUser = "UpdateUser";
        public const string GetAllUserGroup = "GetAllUserGroup";
        public const string ChangePassword = "ChangePassword";
    }
}
