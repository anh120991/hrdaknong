﻿using System;

namespace Base.Model
{
    public class BaseRequestModel
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}