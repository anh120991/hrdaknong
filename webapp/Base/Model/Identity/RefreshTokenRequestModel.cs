﻿namespace Base.Model.Identity
{
    public class RefreshTokenRequestModel
    {
        public string RefreshToken { get; set; }

        public string ClientId { get; set; }

        public string ClientSecret { get; set; }

        public string AccessToken { get; set; }
    }
}