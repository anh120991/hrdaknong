﻿using System;

namespace Base.Model.Menu
{
    public class MenuRequestModel
    {
        public long MenuId { get; set; }
        public string MenuName { get; set; }
        public long MenuParentId { get; set; }
        public int IsActived { get; set; }
        public string MenuDescription { get; set; }
        public int OrderBy { get; set; }
        public string PermissionId { get; set; }
        public string Path { get; set; }
        public long UserId { get; set; }
    }
}
