﻿using System;

namespace Base.Model.Report
{
    public class HcStatusRequestModel
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
