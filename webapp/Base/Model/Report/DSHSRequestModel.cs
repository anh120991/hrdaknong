﻿    using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Model.Report
{
    public class DSHSRequestModel
    {
        public DateTime? fromDate { get; set; }
        public DateTime? toDate { get; set; }
        public int? donVi { get; set; }
        public int? chucVu { get; set; }
        public int? trangThai { get; set; }
    }
}
