﻿    using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Model.Report
{
    public class DVKTKLRequestModel
    {
        public DateTime? V_FRM_DATE { get; set; }
        public DateTime? V_TO_DATE { get; set; }
        public int? V_SOHIEUCBCCVC { get; set; }
        public int V_DONVI { get; set; }
        public int? V_LOAI { get; set; }
    }
}
