﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Model.Permission
{
    public class PermissionRequestModel : BaseModel
    {
        public long Id { get; set; }
        public string PermissionName { get; set; }
        public string PermissionCode { get; set; }
        public long ParentId { get; set; }
        public string Description { get; set; }
        public long DisplayOrder { get; set; }
        public int IsActive { get; set; }
    }
}
