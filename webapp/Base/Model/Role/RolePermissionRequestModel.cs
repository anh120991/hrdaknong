﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Model.Role
{
    public class RolePermissionRequestModel
    {
        public long RoleId { get; set; }
        public long PermissionId { get; set; }
        public bool IsEnabled { get; set; }
    }
}
