﻿using Base.Model.Permission;
using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Model.Role
{
    public class RoleRequestModel : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<RolePermissionRequestModel> PermissionIds { get; set; }
    }
}
