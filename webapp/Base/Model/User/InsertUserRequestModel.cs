﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Base.Model.User
{
    public class InsertUserRequestModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Avatar { get; set; }
        public long Id { get; set; }
        public int Gender { get; set; }

        public List<int> RoleIds { get; set; }
        public List<int> DepartmentIds { get; set; }
        public List<int> GroupIds { get; set; }
    }
}
