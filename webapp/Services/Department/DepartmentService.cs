﻿using Base.Common;
using Base.Constants;
using Services.Common;
using Services.Token;
using System.Threading.Tasks;
using Base.Model.Report;

namespace Services.Department
{
    public class DepartmentService : ClientServiceRequestBase, IDepartmentService
    {
        private const string ApiGroupName = "Department_Api";
        public DepartmentService(ITokenBaseService tokenBaseService = null) :
            base(ApiGroupName, tokenBaseService)
        {
        }

        public async Task<GenericResult<object>> GetBTPK()
        {
            return await GetAsync<GenericResult<object>>(Functions[DepartmentConstant.GetBTDKP], token: TokenString);

        }

        public async Task<GenericResult<object>> GetSubDepartment()
        {
            return await GetAsync<GenericResult<object>>(Functions[DepartmentConstant.GetSubDepartment], token: TokenString);
        }

        public async Task<GenericResult<object>> GetTotalSubDepartmentAsync(HcStatusRequestModel model)
        {
            return await PostAsync<GenericResult<object>, HcStatusRequestModel>(model, Functions[DepartmentConstant.GetTotalSubDepartment], token: TokenString);
        }

        public async Task<GenericResult<object>> GetDepartmentMajorsAsync()
        {
            return await GetAsync<GenericResult<object>>(Functions[DepartmentConstant.GetDepartmentMajors], token: TokenString);
        }

        public async Task<GenericResult<object>> GetTotalSubDepartmentMajorsAsync(HcStatusRequestModel model)
        {
            return await PostAsync<GenericResult<object>, HcStatusRequestModel>(model, Functions[DepartmentConstant.GetTotalSubDepartmentMajors], token: TokenString);
        }

        public async Task<GenericResult<object>> GetTotalPharmaSubDepartmentAsync(HcStatusRequestModel model)
        {
            return await PostAsync<GenericResult<object>, HcStatusRequestModel>(model, Functions[DepartmentConstant.GetTotalPharmaSubDepartment], token: TokenString);
        }

        public async Task<GenericResult<object>> GetTotalEmPatientSubjectAsync(HcStatusRequestModel model)
        {
            return await PostAsync<GenericResult<object>, HcStatusRequestModel>(model, Functions[DepartmentConstant.GetTotalEmPatientSubject], token: TokenString);
        }
        public async Task<GenericResult<object>> GetTotalEmTreatmentAsync(HcStatusRequestModel model)
        {
            return await PostAsync<GenericResult<object>, HcStatusRequestModel>(model, Functions[DepartmentConstant.GetTotalEmTreatment], token: TokenString);
        }
    }
}
