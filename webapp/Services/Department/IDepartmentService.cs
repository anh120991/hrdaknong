﻿using Base.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Base.Model.Report;

namespace Services.Department
{
    public interface IDepartmentService
    {
        Task<GenericResult<object>> GetBTPK();
        Task<GenericResult<object>> GetSubDepartment();
        Task<GenericResult<object>> GetTotalSubDepartmentAsync(HcStatusRequestModel model);
        Task<GenericResult<object>> GetDepartmentMajorsAsync();
        Task<GenericResult<object>> GetTotalSubDepartmentMajorsAsync(HcStatusRequestModel model);
        Task<GenericResult<object>> GetTotalPharmaSubDepartmentAsync(HcStatusRequestModel model);
        Task<GenericResult<object>> GetTotalEmPatientSubjectAsync(HcStatusRequestModel model);
        Task<GenericResult<object>> GetTotalEmTreatmentAsync(HcStatusRequestModel model);
    }
}
