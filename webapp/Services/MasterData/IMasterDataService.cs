﻿using Base.Common;
using Base.Model.Chart;
using Base.Model.Report;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.MasterData
{
   public interface IMasterDataService
    {
        Task<GenericResult<object>> DM_DonVi();
        Task<GenericResult<object>> DM_Chucvu();
        Task<GenericResult<object>> DM_TSHS();
        Task<GenericResult<object>> loadDSHS(DSHSRequestModel model);
    }
}
