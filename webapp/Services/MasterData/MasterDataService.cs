﻿using Base.Common;
using Base.Constants;
using Base.Model.Report;
using Services.Common;
using Services.MasterData;
using Services.Token;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Chart
{
    public class MasterDataService : ClientServiceRequestBase, IMasterDataService
    {
        private const string ApiGroupName = "MasterData_Api";
        public MasterDataService(ITokenBaseService tokenBaseService = null) :
            base(ApiGroupName, tokenBaseService)
        {
        }

        public async Task<GenericResult<object>> DM_Chucvu()
        {
            return await GetAsync<GenericResult<object>>(Functions[MasterDataConstant.GetChucVu], token: TokenString);
        }

        public async Task<GenericResult<object>> DM_DonVi()
        {
            return await GetAsync<GenericResult<object>>(Functions[MasterDataConstant.GetDonVi], token: TokenString);
        }

        public async Task<GenericResult<object>> DM_TSHS()
        {
            return await GetAsync<GenericResult<object>>(Functions[MasterDataConstant.GetTrangThaiHS], token: TokenString);
        }

        public async Task<GenericResult<object>> loadDSHS(DSHSRequestModel model)
        {
            return await PostAsync<GenericResult<object>, DSHSRequestModel>(model, Functions[MasterDataConstant.GetHS], token: TokenString);
        }
    }
}
