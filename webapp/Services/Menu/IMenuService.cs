﻿using Base.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Base.Model.Report;
using Base.Model.Menu;

namespace Services.Menu
{
    public interface IMenuService
    {
        Task<GenericResult<object>> GetMenu();
        Task<GenericResult<object>> CreateMenu(MenuRequestModel model);
        Task<GenericResult<object>> UpdateMenu(MenuRequestModel model);
        Task<GenericResult<object>> GetMenuDetail(long id);
    }
}
