﻿using Base.Common;
using Base.Constants;
using Services.Common;
using Services.Token;
using System.Threading.Tasks;
using Base.Model.Report;
using Base.Model.Menu;

namespace Services.Menu
{
    public class MenuService : ClientServiceRequestBase, IMenuService
    {
        private const string ApiGroupName = "Menu_Api";
        public MenuService(ITokenBaseService tokenBaseService = null) :
            base(ApiGroupName, tokenBaseService)
        {
        }

        public async Task<GenericResult<object>> CreateMenu(MenuRequestModel model)
        {
            return await PostAsync<GenericResult<object>, MenuRequestModel>(model, Functions[MenuConstant.CreateMenu], token: TokenString);
        }

        public async Task<GenericResult<object>> GetMenu()
        {
            return await GetAsync<GenericResult<object>>(Functions[MenuConstant.GetMenu], token: TokenString);
        }

        public async Task<GenericResult<object>> GetMenuDetail(long id)
        {
            return await GetAsync<GenericResult<object>>(string.Format(Functions[MenuConstant.GetMenuDetail], id), token: TokenString);
        }

        public async Task<GenericResult<object>> UpdateMenu(MenuRequestModel model)
        {
            return await PostAsync<GenericResult<object>, MenuRequestModel>(model, Functions[MenuConstant.UpdateMenu], token: TokenString);
        }
    }
}
