﻿using Base.Common;
using Base.Model.Permission;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Permission
{
    public interface IPermissionService
    {
        Task<GenericResult<object>> CreatePermission(PermissionRequestModel model);
        Task<GenericResult<object>> UpdatePermission(PermissionRequestModel model);
        Task<GenericResult<object>> GetAllPermission();
        Task<GenericResult<object>> GetDetailPermission(long id);
    }
}
