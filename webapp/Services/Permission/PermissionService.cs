﻿using Base.Common;
using Base.Constants;
using Base.Model.Chart;
using Base.Model.Permission;
using Base.Model.User;
using Services.Common;
using Services.Token;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Permission
{
    public class PermissionService : ClientServiceRequestBase, IPermissionService
    {
        private const string ApiGroupName = "Permission_Api";
        public PermissionService(ITokenBaseService tokenBaseService = null) :
            base(ApiGroupName, tokenBaseService)
        {
        }

        public async Task<GenericResult<object>> CreatePermission(PermissionRequestModel model)
        {
            return await PostAsync<GenericResult<object>, PermissionRequestModel>(model, Functions[PermissionConstant.AddNewPermission], token: TokenString);
        }

        public async Task<GenericResult<object>> GetAllPermission()
        {
            return await GetAsync<GenericResult<object>>(Functions[PermissionConstant.GetAllPermission], token: TokenString);
        }

        public async Task<GenericResult<object>> GetDetailPermission(long id)
        {
            return await GetAsync<GenericResult<object>>(string.Format(Functions[PermissionConstant.GetDetailPermission], id), token: TokenString);
        }

        public async Task<GenericResult<object>> UpdatePermission(PermissionRequestModel model)
        {
            return await PostAsync<GenericResult<object>, PermissionRequestModel>(model, Functions[PermissionConstant.UpdatePermission], token: TokenString);
        }
    }
}
