﻿using Base.Common;
using Base.Model.Report;
using System.Threading.Tasks;

namespace Services.Report
{
    public interface IReportService
    {
        Task<GenericResult<object>> Thongkedangvien();
        Task<GenericResult<object>> Dotuoi();
        Task<GenericResult<object>> Thongkenhansu();
        Task<GenericResult<object>> Thongkenhansu_Donvi();
        Task<GenericResult<object>> Trinhdotinhoc();
        Task<GenericResult<object>> Tilekhenthuong();
        Task<GenericResult<object>> Tilekyluat();
        Task<GenericResult<object>> Trinhdohocvan();
         Task<GenericResult<object>> Trinhdongoaingu();
        Task<GenericResult<object>> Thongkecbns();
        Task<GenericResult<object>> Donviktkl();
        Task<GenericResult<object>> DSktkl(DVKTKLRequestModel model);
    }
}
