﻿using Base.Common;
using Base.Constants;
using Base.Model.Chart;
using Base.Model.Report;
using Base.Model.Role;
using Base.Model.User;
using Services.Common;
using Services.Token;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Report
{
    public class ReportService : ClientServiceRequestBase, IReportService
    {
        private const string ApiGroupName = "Report_Api";
        public ReportService(ITokenBaseService tokenBaseService = null) :
            base(ApiGroupName, tokenBaseService)
        {
        }

        public async Task<GenericResult<object>> Donviktkl()
        {
            return await GetAsync<GenericResult<object>>(Functions[ReportConstant.Donviktkl], token: TokenString);
        }

        public async Task<GenericResult<object>> Dotuoi()
        {
            return await GetAsync<GenericResult<object>>(Functions[ReportConstant.Dotuoi], token: TokenString);
        }

        public async Task<GenericResult<object>> DSktkl(DVKTKLRequestModel model)
        {
            return await PostAsync<GenericResult<object>, DVKTKLRequestModel>(model, Functions[ReportConstant.DSktkl], token: TokenString);
        }

        public async Task<GenericResult<object>> Thongkecbns()
        {
            return await GetAsync<GenericResult<object>>(Functions[ReportConstant.Thongkecbns], token: TokenString);
        }

        public async Task<GenericResult<object>> Thongkedangvien()
        {
            return await GetAsync<GenericResult<object>>(Functions[ReportConstant.Thongkedangvien], token: TokenString);
        }

        public async Task<GenericResult<object>> Thongkenhansu()
        {
            return await GetAsync<GenericResult<object>>(Functions[ReportConstant.Thongkenhansu], token: TokenString);
        }

        public async Task<GenericResult<object>> Thongkenhansu_Donvi()
        {
            return await GetAsync<GenericResult<object>>(Functions[ReportConstant.ThongkenhansuDonvi], token: TokenString);
        }

        public async Task<GenericResult<object>> Tilekhenthuong()
        {
            return await GetAsync<GenericResult<object>>(Functions[ReportConstant.Tilekhenthuong], token: TokenString);
        }

        public async Task<GenericResult<object>> Tilekyluat()
        {
            return await GetAsync<GenericResult<object>>(Functions[ReportConstant.Tilekyluat], token: TokenString);
        }

        public async Task<GenericResult<object>> Trinhdohocvan()
        {
            return await GetAsync<GenericResult<object>>(Functions[ReportConstant.Trinhdohocvan], token: TokenString);
        }

        public async Task<GenericResult<object>> Trinhdongoaingu()
        {
            return await GetAsync<GenericResult<object>>(Functions[ReportConstant.Trinhdongoaingu], token: TokenString);
        }

        public async Task<GenericResult<object>> Trinhdotinhoc()
        {
            return await GetAsync<GenericResult<object>>(Functions[ReportConstant.Trinhdotinhoc], token: TokenString);
        }
    }
}
