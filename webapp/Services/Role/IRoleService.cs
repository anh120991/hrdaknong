﻿using Base.Common;
using Base.Model.Role;
using Base.Model.User;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Role
{
    public interface IRoleService
    {
        Task<GenericResult<object>> CreateRole(RoleRequestModel model);
        Task<GenericResult<object>> UpdateRole(RoleRequestModel model);
        Task<GenericResult<object>> GetAll();
        Task<GenericResult<object>> GetDetailRole(long id);
    }
}
