﻿using Base.Common;
using Base.Constants;
using Base.Model.Chart;
using Base.Model.User;
using Services.Common;
using Services.Token;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Department
{
    public class UserService : ClientServiceRequestBase, IUserService
    {
        private const string ApiGroupName = "User_Api";
        public UserService(ITokenBaseService tokenBaseService) :
            base(ApiGroupName, tokenBaseService)
        {
        }

        public async Task<GenericResult<object>> ChangePassWord(UserChangePasswordRequestModel model)
        {
            return await PostAsync<GenericResult<object>, UserChangePasswordRequestModel>(model, Functions[UserConstant.ChangePassword], token: TokenString);

        }

        public async Task<GenericResult<object>> CreateNewUser(InsertUserRequestModel model)
        {
            return await PostAsync<GenericResult<object>, InsertUserRequestModel>(model, Functions[UserConstant.CreateNewUser], token: TokenString);

        }

        public async Task<GenericResult<object>> GetAllUser()
        {
            return await GetAsync<GenericResult<object>>(Functions[UserConstant.GetAllUser], token: TokenString);
        }

        public async Task<GenericResult<object>> GetAllUserGroup()
        {
            return await GetAsync<GenericResult<object>>(Functions[UserConstant.GetAllUserGroup], token: TokenString);
        }

        public async Task<GenericResult<object>> GetDetailUser(long id)
        {
            return await GetAsync<GenericResult<object>>(string.Format(Functions[UserConstant.GetDetailUser], id), token: TokenString);
        }

        public async Task<GenericResult<object>> UpdateUser(InsertUserRequestModel model)
        {
            return await PostAsync<GenericResult<object>, InsertUserRequestModel>(model, Functions[UserConstant.UpdateUser], token: TokenString);
        }
    }
}
