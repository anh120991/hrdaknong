﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Base.Common;
using Base.Constants;
using Base.Model.Authentication;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using Services.Authentication;
using Services.Token;

namespace WebApp.Controllers
{

    public class AccountController : Controller
    {
        private readonly IAuthenticationRestClient _identityService;

        public AccountController(IAuthenticationRestClient identityService)
        {
            _identityService = identityService;
        }

        // GET
        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl = "")
        {
            if (HttpContext.User != null && HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToLocal(returnUrl);
            }

            var model = new LoginViewModel()
            {
                ReturnUrl = returnUrl,
            };

            return View(model);
        }

        public JsonResult LoginSubmit(string userName, string passWord)
        {
            //var result = await _chartService.GetChartOverview(new OverviewRequestModel
            //{
            //    FromDate = searchDate,
            //    Makp = mpk
            //});
            if(userName.Trim().ToUpper() == "ADMIN" && passWord == "admin")
            {
                return Json(1);
            }
            return Json(0);
            // return await _chartService.GetChartOverview(model);
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var requestAccessToken = _identityService.GetToken(new LoginRequestModel()
            {
                Email = model.UserAccount,
                Username = model.UserAccount,
                Password = model.UserSecret,
            });

            var result = await LoginLogic(requestAccessToken, model);
            if (result)
            {
                return RedirectToLocal(model.ReturnUrl);
            }

            ModelState.AddModelError(string.Empty, requestAccessToken.Metadata.Message);
            model.errorMessage = "Tài khoản hoặc mật khẩu không đúng";
            return View(model);
        }

        private ActionResult RedirectToLocal(string returnUrl = "/Home/Index")
        {
            if (Url.IsLocalUrl(returnUrl) && returnUrl.ToLower().IndexOf("logout") < 0)
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        private async Task<bool> LoginLogic(GenericResult<LoginResponseModel> requestAccessToken, LoginViewModel model)
        {
            if (requestAccessToken.Success)
            {
                var hand = new JwtSecurityTokenHandler();
                var tokenString = requestAccessToken.First().Token;
                var tokenInfo = hand.ReadJwtToken(tokenString);

                string userId = tokenInfo.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;
                var userInfor = new LoggedUserDetailResponseModel();
                var result = await _identityService.GetCurrentUserDetailAsync(tokenString);
                if (result.Metadata.Success)
                {
                    userInfor = result.Results[0];
                    if (userInfor.Avatar == null)
                    {
                        userInfor.Avatar = string.Empty;
                    }
                }

                ClaimsIdentity identityClaim = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
                identityClaim.AddClaims(
                new List<Claim>()
                {
                    new Claim(ClaimTypes.Name, model.UserAccount ?? tokenInfo.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name).Value),
                    new Claim(ClaimTypes.NameIdentifier, userId),
                    new Claim(ClaimTypes.Actor, GetClaimFromLoginResponseModel(requestAccessToken)),
                    new Claim(ApplicationKey.Token, JsonConvert.SerializeObject(requestAccessToken.First())),
                    new Claim(ApplicationKey.UserInfo, JsonConvert.SerializeObject(userInfor))
                });
                ClaimsPrincipal principal = new ClaimsPrincipal(identityClaim);

                await HttpContext.SignInAsync(
                 CookieAuthenticationDefaults.AuthenticationScheme,
                 principal,
                 new AuthenticationProperties
                 {
                     IsPersistent = false
                 });

                return true;
            }

            return false;
        }

        private string GetClaimFromLoginResponseModel(GenericResult<LoginResponseModel> result, string claimTypes = ClaimTypes.NameIdentifier)
        {
            var model = result.Results.First();
            if (model == null || string.IsNullOrEmpty(model.Token))
            {
                return null;
            }

            return GetClaimFromToken(model.Token, claimTypes);
        }

        private string GetClaimFromToken(string token, string claimTypes = ClaimTypes.NameIdentifier)
        {
            var jwtToken = new JwtSecurityToken(token);
            var userIdClaim = jwtToken.Claims.Where(x => x.Type == claimTypes).First();

            if (userIdClaim == null || string.IsNullOrEmpty(userIdClaim.Value))
            {
                return null;
            }

            return userIdClaim.Value;
        }


        [Authorize]
        public async Task<IActionResult> Logout()
        {
            try
            {
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

                var appToken = GetTokenFromClaim();
                if (appToken != null)
                {
                    if ((appToken.ExpiresIn - DateTime.UtcNow).TotalMinutes > 0 && !string.IsNullOrEmpty(appToken.Token))
                    {
                        _identityService.Logout(appToken.Token);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return RedirectToAction("Login", "Account");
        }

        private AppToken GetTokenFromClaim()
        {
            var userIdentity = (ClaimsIdentity)User.Identity;
            var apiClaim = userIdentity.FindFirst(ApplicationKey.Token);
            if (apiClaim != null)
            {
                var appToken = JsonConvert.DeserializeObject<AppToken>(apiClaim.Value);
                return appToken;
            }

            return null;
        }
    }
}  