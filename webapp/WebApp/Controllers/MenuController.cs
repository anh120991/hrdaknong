﻿using Base.Constants;
using Base.Model.Menu;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Helper;
using WebApp.Models.DataModels;

namespace WebApp.Controllers
{
    public class MenuController : Controller
    {
        private IMenuService _menuService;

        public MenuController(IMenuService menuService)
        {
            _menuService = menuService;
        }
        public IActionResult Index()
        {
            if (!User.IsAllow(PermissionKeyConstant.IsViewMenu))
            {
                return RedirectToAction("NoAuthentication", "system");
            }
            var res = _menuService.GetMenu();
            List<MenuViewModel> lsResult = new List<MenuViewModel>();
            if(res != null && res.Result.Results.Any())
            {
               foreach(var str in res.Result.Results)
                {
                    var item = JsonConvert.DeserializeObject<MenuViewModel>(str.ToString());
                    lsResult.Add(item);
                }
            }
            return View(lsResult);
        }

        private List<MenuViewModel> getRootMenu()
        {
            var res = _menuService.GetMenu();
            List<MenuViewModel> lsResult = new List<MenuViewModel>();
            if (res != null && res.Result.Results.Any())
            {
                foreach (var str in res.Result.Results)
                {
                    var item = JsonConvert.DeserializeObject<MenuViewModel>(str.ToString());
                    if (item.MENU_PARENT_ID == 0)
                        lsResult.Add(item);
                }
            }
            return lsResult;
        }

        public IActionResult Create(long menuId = -1)
        {
            MenuViewModel model = new MenuViewModel();
            if(menuId > 0)
            {
                model.MENU_PARENT_ID = menuId;
            }
            ViewBag.ListMenu = getRootMenu();
            return View(model);
        }

        public async Task<JsonResult> InsertMenu(MenuRequestModel data)
        {
            data.UserId = 1;
            var result = await _menuService.CreateMenu(data);
            return Json(result);
        }

        public async Task<IActionResult> Update(long menuId = -1)
        {
            ViewBag.ListMenu = getRootMenu();
            MenuViewModel model = new MenuViewModel();
            var rs = await _menuService.GetMenuDetail(menuId);
            if (rs != null)
            {
                model = JsonConvert.DeserializeObject<MenuViewModel>(rs.Results[0].ToString());
            }
            ViewBag.ViewMode = "UPDATE";
            return View("Create", model);
        }


        public async Task<JsonResult> UpdateMenu(MenuRequestModel data)
        {
            data.UserId = 1;
            var result = await _menuService.UpdateMenu(data);
            return Json(result);
        }
    }
}
