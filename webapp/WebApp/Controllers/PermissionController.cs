﻿using Base.Constants;
using Base.Model.Permission;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Permission;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Helper;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class PermissionController : Controller
    {
        IPermissionService _permissionService;
        string _permissionList = "tmpPermissionList";
        public PermissionController(IPermissionService permissionService)
        {
            _permissionService = permissionService;
        }
        public async Task<IActionResult> Index()
        {
            if (!User.IsAllow(PermissionKeyConstant.IsViewMenuPermission))
            {
                return RedirectToAction("NoAuthentication", "system");
            }
            var result = await _permissionService.GetAllPermission();
            List<permissionViewModel> lsResult = new List<permissionViewModel>();
            if (result != null && result.Results.Count > 0)
            {
                foreach (var item in result.Results)
                {
                    permissionViewModel _dep = new permissionViewModel();
                    _dep = JsonConvert.DeserializeObject<permissionViewModel>(item.ToString());
                    lsResult.Add(_dep);
                }

            }
           
            return View(lsResult);
        }

        public IActionResult Create()
        {
            permissionViewModel model = new permissionViewModel();
            return View(model);
        }

        public async Task<IActionResult> Update(long permission)
        {
            permissionViewModel model = new permissionViewModel();
            var rs = await _permissionService.GetDetailPermission(permission);
            if (rs != null)
            {
                model = JsonConvert.DeserializeObject<permissionViewModel>(rs.Results[0].ToString());
            }
            ViewBag.IsUpdate = 1;
            return View("Create", model);
        }

        public async Task<JsonResult> InsertPermission(PermissionRequestModel data)
        {

            var result = await _permissionService.CreatePermission(data);
            return Json(result);
        }

        public async Task<JsonResult> UpdatePermission(PermissionRequestModel data)
        {

            var result = await _permissionService.UpdatePermission(data);
            return Json(result);
        }
    }
}
