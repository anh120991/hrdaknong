﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Base.Model.Report;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Report;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Authorize]
    public class ReportController : Controller
    {
        IReportService _reportService;
        public ReportController(IReportService reportService)
        {
            _reportService = reportService;
        }

        [HttpGet]
        public async Task<IActionResult> Thongkedangvien()
        {

            var result = await _reportService.Thongkedangvien();
            return Json(result);
        }
        [HttpGet]
        public async Task<IActionResult> Dotuoi()
        {

            var result = await _reportService.Dotuoi();
            return Json(result);
        }
        [HttpGet]
        public async Task<IActionResult> Thongkenhansu()
        {

            var result = await _reportService.Thongkenhansu();
            return Json(result);
        }
        [HttpGet]
        public async Task<IActionResult> Thongkenhansu_Donvi()
        {

            var result = await _reportService.Thongkenhansu_Donvi();
            return Json(result);
        }
        [HttpGet]
        public async Task<IActionResult> Trinhdotinhoc()
        {

            var result = await _reportService.Trinhdotinhoc();
            return Json(result);
        }
        [HttpGet]
        public async Task<IActionResult> Tilekhenthuong()
        {

            var result = await _reportService.Tilekhenthuong();
            return Json(result);
        }
        [HttpGet]
        public async Task<IActionResult> Tilekyluat()
        {

            var result = await _reportService.Tilekyluat();
            return Json(result);
        }
        [HttpGet]
        public async Task<IActionResult> Trinhdohocvan()
        {

            var result = await _reportService.Trinhdohocvan();
            return Json(result);
        }
        [HttpGet]
        public async Task<IActionResult> Trinhdongoaingu()
        {

            var result = await _reportService.Trinhdongoaingu();
            return Json(result);
        }
        [HttpGet]
        public async Task<IActionResult> Thongkecbns()
        {

            var result = await _reportService.Thongkecbns();
            return Json(result);
        }
        [HttpGet]
        public async Task<IActionResult> Donviktkl()
        {

            var result = await _reportService.Donviktkl();
            return Json(result);
        }

        public IActionResult DSktkl(int MaDonVi, int Loai)
        {
            ViewBag.Madonvi = MaDonVi;
            ViewBag.Loai = Loai;
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> load_DSktkl(int madonvi, int loai)
        {

            DVKTKLRequestModel model = new DVKTKLRequestModel();
            model.V_DONVI = madonvi;
            model.V_LOAI = loai;
            var result = await _reportService.DSktkl(model);
            return Json(result);
        }
    }
}
