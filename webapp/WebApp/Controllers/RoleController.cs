﻿using Base.Constants;
using Base.Model.Role;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Helper;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class RoleController : Controller
    {
        IRoleService _roleService;
        public RoleController(IRoleService roleService)
        {
            
            _roleService = roleService;
        }

        public async Task<IActionResult> Index()
        {
            if (!User.IsAllow(PermissionKeyConstant.IsViewMenuRole))
            {
                return RedirectToAction("NoAuthentication", "system");
            }
            var result = await _roleService.GetAll();
            List<roleViewModel> lsResult = new List<roleViewModel>();
            if (result != null && result.Results.Count > 0)
            {
                foreach (var item in result.Results)
                {
                    roleViewModel _dep = new roleViewModel();
                    _dep = JsonConvert.DeserializeObject<roleViewModel>(item.ToString());
                    lsResult.Add(_dep);
                }

            }
            return View(lsResult);
        }
         
        public IActionResult Create()
        {
            roleViewModel model = new roleViewModel();
            return View(model);
        }

        public async Task<IActionResult>  Update(long roleId = -1)
        {
            roleViewModel model = new roleViewModel();
            var rs = await _roleService.GetDetailRole(roleId);
            if(rs != null)
            {
                model = JsonConvert.DeserializeObject<roleViewModel>(rs.Results[0].ToString());
            }
            return View("Create", model);
        }

        public async Task<JsonResult> InsertRole(RoleRequestModel data)
        {
            var result = await _roleService.CreateRole(data);
            return Json(result);
        }
        public async Task<JsonResult> UpdateRole(RoleRequestModel data)
        {
            var result = await _roleService.UpdateRole(data);
            return Json(result);
        }
    }
}
