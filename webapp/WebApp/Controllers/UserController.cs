﻿using Base.Constants;
using Base.Model.Report;
using Base.Model.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Department;
using Services.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Common;
using WebApp.Helper;
using WebApp.Models;
using WebApp.Models.DataModels;


namespace WebApp.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private IUserService _userService;
        private IMasterDataService _masterDataService;
        public UserController(IUserService userService, IMasterDataService masterDataService) {
            _userService = userService;
            _masterDataService = masterDataService;
        }
        public async Task<IActionResult> Index()
        {
            if (!User.IsAllow(PermissionKeyConstant.IsViewMenuUser))
            {
                return RedirectToAction("NoAuthentication", "system");
            }
            var result = await _userService.GetAllUser();
            List<userViewModel> lsResult = new List<userViewModel>();
            if (result != null && result.Results.Count > 0)
            {
                foreach (var item in result.Results)
                {
                    userViewModel _dep = new userViewModel();
                    _dep = JsonConvert.DeserializeObject<userViewModel>(item.ToString());
                    lsResult.Add(_dep);
                }

            }
            return View(lsResult);
        }

        public IActionResult Create()
        {
            userViewModel model = new userViewModel();
            return View(model);
        }

        public IActionResult ChangePassword()
        {
            return View();
        }

        public async Task<IActionResult> Update(long id)
        {
            var result = await _userService.GetDetailUser(id);
            userViewModel model = new userViewModel();
            if (result != null)
            {
                model = JsonConvert.DeserializeObject<userViewModel>(result.Results[0].ToString());
            }
            ViewBag.ViewMode = "UPDATE";
            return View("Create", model);
        }

        public async Task<JsonResult> InsertUser(UserModel data)
        {
            InsertUserRequestModel requestModel = new InsertUserRequestModel();
            string rdPass = Uitil.RandomString(10);
            data.Password = rdPass;
            Uitil.TransferData(data, ref requestModel);
            var result = await _userService.CreateNewUser(requestModel);
            result.Metadata.Message = rdPass;
            return Json(result);
        }

        public async Task<JsonResult> UpdateUser(UserModel data)
        {
            InsertUserRequestModel requestModel = new InsertUserRequestModel();
            Uitil.TransferData(data, ref requestModel);
            var result = await _userService.UpdateUser(requestModel);
            return Json(result);
        }

        [HttpPost]
        public async Task<JsonResult> ChangePassWord(UserChangePasswordRequestModel data)
        {
            string strTempId = User.Claims.FirstOrDefault(c => c.Type == "Id").Value;
            data.Id = !string.IsNullOrEmpty(strTempId) ? long.Parse(strTempId) : -1;
            if(data.Id > 0)
            {
                var result = await _userService.ChangePassWord(data);
                if (result.Success)
                {
                    return Json(1);
                }
            }
            
            return Json(0);
        }

        public IActionResult Employees()
        {
            return View();
        }

        public async Task<JsonResult> TTHS()
        {
            var status = await _masterDataService.DM_TSHS();
            return Json(status);
        }
        public async Task<JsonResult> DM_DonVi()
        {
            var donvi = await _masterDataService.DM_DonVi();
            return Json(donvi);
        }
        public async Task<JsonResult> DM_Chucvu()
        {
            var chucvu = await _masterDataService.DM_Chucvu();
            return Json(chucvu);
        }

        [HttpGet]
        public async Task<JsonResult> loadHS(int? donVi, int? chucVu, int? trangThai)
        {
            var model = new DSHSRequestModel()
            {
                chucVu = chucVu,
                donVi = donVi,
                trangThai = trangThai
            };
            var chucvu = await _masterDataService.loadDSHS(model);
            return Json(chucvu);
        }
    }
}
