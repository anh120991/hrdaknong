﻿using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace WebApp.Helper
{
    public static class ClaimsPrincipalExtensions
    {
        public static bool IsAllow(this ClaimsPrincipal claimsPrincipal, params string[] roles)
        {
            return claimsPrincipal.Claims.Any(r => r.Type == ClaimTypes.Role && roles.Contains(r.Value));
        }

        public static string GetValue(this ClaimsPrincipal claimsPrincipal, string key)
        {
            var result = claimsPrincipal.Claims.FirstOrDefault(r => r.Type == key);
            return result?.Value ?? string.Empty;
        }
    }
}