﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Base.Common;
using Base.Constants;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace WebApp.Middleware
{
    public class ExtractPermissionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExtractPermissionMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task InvokeAsync(HttpContext httpContext)
        {
            TryExtractPermission(httpContext);
            await _next(httpContext);
        }

        private void TryExtractPermission(HttpContext httpContext)
        {
            try
            {
                var apiClaim = httpContext.User.FindFirst(ApplicationKey.Token);
                if (apiClaim != null)
                {
                    var appToken = JsonConvert.DeserializeObject<AppToken>(apiClaim.Value);
                    var token = appToken.Token;
                    var hand = new JwtSecurityTokenHandler();
                    var tokenInfo = hand.ReadJwtToken(token);

                    var roleClaims = tokenInfo.Claims.Where(r => r.Type == ClaimTypes.Role);
                    var appIdentity = new ClaimsIdentity(roleClaims);
                    httpContext.User.AddIdentity(appIdentity);
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}