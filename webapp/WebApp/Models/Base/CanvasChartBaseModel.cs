﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models.Canvas;

namespace WebApp.Models
{
    public class CanvasChartBaseModel
    {
        public CanvasChartBaseModel()
        {
            dataPoints = new List<DataPointModel>();
        }
        public string indexLabel { get; set; }
        public string type { get; set; }
        public bool? showInLegend { get; set; }
        public string yValueFormatString { get; set; }
        public string name { get; set; }
        public string toolTipContent { get; set; }
        public List<DataPointModel> dataPoints { get; set; }
    }
}
