﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.Canvas
{
    public class DataPointModel
    {
        public string label { get; set; }
        public string name { get; set; }
        public decimal y { get; set; }
        public string x { get; set; }
        public string color { get; set; }

    }
}
