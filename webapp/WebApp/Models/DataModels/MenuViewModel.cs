﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models.DataModels
{
    public class MenuViewModel
    {
        public long MENU_ID { get; set; }
        public string MENU_NAME { get; set; }
        public long MENU_PARENT_ID { get; set; }
        public int ISACTIVED { get; set; }
        public string MENU_DESCRIPTION { get; set; }
        public int ORDERBY { get; set; }
        public int PERMISSION_ID { get; set; }
        public string PATH { get; set; }
        public string CREATEDUSER { get; set; }
        public DateTime? CREATEDDATE { get; set; }
        public string UPDATEDUSER { get; set; }
        public DateTime? UPDATEDDATE { get; set; }
    }
}
