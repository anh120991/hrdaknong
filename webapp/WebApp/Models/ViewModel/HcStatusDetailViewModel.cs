﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class HcStatusDetailViewModel
    {
        public DateTime oP_DATE { get; set; }
        public long oP_HCS_ID { get; set; }
        public int oP_PATIENTS { get; set; }
        public int oP_EXAMS { get; set; }
    }

    public class HcStatusChartViewModel : HcStatusDetailViewModel
    {
        public int Sum_PATIENTS { get; set; }
        public int Sum_EXAMS { get; set; }
    }
}
