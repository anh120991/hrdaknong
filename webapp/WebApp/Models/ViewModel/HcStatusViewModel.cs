﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class HcStatusViewModel
    {
        public long hcS_ID { get; set; }
        public string hcS_CODE { get; set; }
        public string hcS_NAME { get; set; }
    }
}
