﻿using Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class permissionViewModel : BaseModel
    {
        public string Code { get; set; }
        public string Parent { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long DisplayOrder { get; set; }
        public bool IsEnabled { get; set; }
    }
}
