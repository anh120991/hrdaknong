﻿using Base.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class roleViewModel : BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public List<RolePermissionViewModel> permissions { get; set; }
    }
}
