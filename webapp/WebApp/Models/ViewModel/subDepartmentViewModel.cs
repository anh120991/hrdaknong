﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class subDepartmentViewModel
    {
        public int MADONVI { get; set; }
        public string TENDONVI { get; set; }
        public int LOAI { get; set; }
        public string MA { get; set; }
        public int TRENCAP { get; set; }
    }
}
