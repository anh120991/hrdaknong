﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class userGroupViewModel
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int IsMultilDepartments { get; set; }
    }
}
