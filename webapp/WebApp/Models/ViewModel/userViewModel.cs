﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class userViewModel
    {
        public long id { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string DepartmentName { get; set; }
        public string Address { get; set; }
        public bool IsActive { get; set; }
        public int Gender { get; set; }
        public List<int> RoleIds { get; set; }
        public List<int> DepartmentIds { get; set; }
        public List<int> GroupIds { get; set; }
    }
}
