﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Department;
using Services.MasterData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using WebApp.Models.Chart;

namespace WebApp.ViewComponents
{
    [ViewComponent(Name = "departmentList")]
    public class departmentList : ViewComponent
	{
        private IMasterDataService _masterDataService;

        public departmentList(IMasterDataService masterDataService)
        {
            _masterDataService = masterDataService;
        }



        public async Task<IViewComponentResult> InvokeAsync(List<int> departmentSelect)
        {
            List<subDepartmentViewModel> lsResult = new List<subDepartmentViewModel>();
            var result = await _masterDataService.DM_DonVi();
            if (result != null && result.Results.Count > 0)
            {
                foreach (var item in result.Results)
                {
                    //departmentViewModel _dep = new departmentViewModel();
                    //_dep = JsonConvert.DeserializeObject<departmentViewModel>(item.ToString());
                    subDepartmentViewModel _dep = new subDepartmentViewModel();
                    _dep = JsonConvert.DeserializeObject<subDepartmentViewModel>(item.ToString());
                    lsResult.Add(_dep);
                }

            }
            ViewBag.Selected = departmentSelect;
            return View(lsResult);
        }
    }
}
