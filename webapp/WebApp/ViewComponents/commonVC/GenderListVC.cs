﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Department;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using WebApp.Models.Chart;


namespace WebApp.ViewComponents
{
    [ViewComponent(Name = "genderList")]
    public class genderList : ViewComponent
	{
        private IDepartmentService _departmentService;

        public genderList(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }



        public IViewComponentResult Invoke(int select = -1)
        {
            ViewBag.Selected = select;
            return View();
        }
    }
}
