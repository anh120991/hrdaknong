﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Department;
using Services.Permission;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using WebApp.Models.Chart;


namespace WebApp.ViewComponents
{
    [ViewComponent(Name = "permissionList")]
    public class permissionList : ViewComponent
	{
        private IPermissionService _permissionService;

        public permissionList(IPermissionService permissionService)
        {
            _permissionService = permissionService;
        }



        public async Task<IViewComponentResult> InvokeAsync(List<string> selected, bool isSelecctBox = false)
        {
            List<permissionViewModel> lsResult = new List<permissionViewModel>();
            var result = await _permissionService.GetAllPermission();
            if (result != null && result.Results.Count > 0)
            {
                foreach (var item in result.Results)
                {
                    permissionViewModel _dep = new permissionViewModel();
                    _dep = JsonConvert.DeserializeObject<permissionViewModel>(item.ToString());
                    lsResult.Add(_dep);
                }

            }
            ViewBag.Selected = selected;
            ViewBag.IsSelecctBox = isSelecctBox;
            return View(lsResult);
        }
    }
}
