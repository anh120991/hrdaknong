﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Department;
using Services.Permission;
using Services.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using WebApp.Models.Chart;


namespace WebApp.ViewComponents
{
    [ViewComponent(Name = "roleList")]
    public class roleList : ViewComponent
	{
        private IRoleService _roleService;

        public roleList(IRoleService roleService)
        {
            _roleService = roleService;
        }



        public async Task<IViewComponentResult> InvokeAsync(List<int> lsSelected)
        {
            var result = await _roleService.GetAll();
            List<roleViewModel> lsResult = new List<roleViewModel>();
            if (result != null && result.Results.Count > 0)
            {
                foreach (var item in result.Results)
                {
                    roleViewModel _dep = new roleViewModel();
                    _dep = JsonConvert.DeserializeObject<roleViewModel>(item.ToString());
                    lsResult.Add(_dep);
                }

            }
            ViewBag.Selected = lsSelected;
            return View(lsResult);
        }
    }
}
