﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Department;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using WebApp.Models.Chart;

namespace WebApp.ViewComponents
{
    [ViewComponent(Name = "usergroupList")]
    public class usergroupList : ViewComponent
	{
        private IUserService _userService;

        public usergroupList(IUserService userService)
        {
            _userService = userService;
        }



        public async Task<IViewComponentResult> InvokeAsync(int userSelect)
        {
            List<userGroupViewModel> lsResult = new List<userGroupViewModel>();
            var result = await _userService.GetAllUserGroup();
            if (result != null && result.Results.Count > 0)
            {
                foreach (var item in result.Results)
                {
                    userGroupViewModel _dep = new userGroupViewModel();
                    _dep = JsonConvert.DeserializeObject<userGroupViewModel>(item.ToString());
                    lsResult.Add(_dep);
                }

            }
            ViewBag.Selected = userSelect;
            return View(lsResult);
        }
    }
}
