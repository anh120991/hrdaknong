﻿
var report = {
    Init: function (element) {

    },
    Thongkedangvien: function (elmCanvas, elmContainer) {


        $.ajax('/report/Thongkedangvien',
            {
                type: 'GET',  // http method
                success: function (data, status, xhr) {   // success callback function
                    if (data !== null && data.results.length > 0) {
                        var res = data.results[0];
                        const percent = parseFloat(res.dangvien / res.tong) * 100;
                        const color = '#01713c';
                        const canvas = elmCanvas;
                        const container = elmContainer;

                        const percentValue = percent; // Sets the single percentage value
                        const colorGreen = color, // Sets the chart color
                            animationTime = '1400'; // Sets speed/duration of the animation

                        const chartCanvas = document.getElementById(canvas), // Sets canvas element by ID
                            chartContainer = document.getElementById(container), // Sets container element ID
                            divElement = document.createElement('div'), // Create element to hold and show percentage value in the center on the chart
                            domString = '<div class="chart__value"><p>' + percentValue + '%</p></div>'; // String holding markup for above created element

                        // Create a new Chart object
                        const doughnutChart = new Chart(chartCanvas, {
                            type: 'doughnut', // Set the chart to be a doughnut chart type
                            data: {
                                datasets: [
                                    {
                                        data: [percentValue, 100 - percentValue], // Set the value shown in the chart as a percentage (out of 100)
                                        backgroundColor: [colorGreen], // The background color of the filled chart
                                        borderWidth: 0 // Width of border around the chart
                                    }
                                ]
                            },
                            options: {
                                cutoutPercentage: 84, // The percentage of the middle cut out of the chart
                                responsive: false, // Set the chart to not be responsive
                                tooltips: {
                                    enabled: false // Hide tooltips
                                }
                            }
                        });

                        Chart.defaults.global.animation.duration = animationTime; // Set the animation duration

                        divElement.innerHTML = domString; // Parse the HTML set in the domString to the innerHTML of the divElement
                        chartContainer.appendChild(divElement.firstChild); // Append the divElement within the chartContainer as it's child
                    }
                
                }
            });

    },
    Thongkedotuoi: function (elm) {


        $.ajax('/report/Dotuoi',
            {
                type: 'GET',  // http method
                success: function (data, status, xhr) {   // success callback function
                    if (data !== null && data.results.length > 0) {
                        var res = data.results[0];

                        var chartDT = new Chart(elm, {
                            type: 'horizontalBar',
                            data: {
                                labels: ['18t - 25t', '26t - 30t', '31t - 35t', '35t - 40t', '41t - 50t', '50t - 60t', '>60t'],
                                datasets: [
                                    {
                                        data: [res.naM_DOTUOI1, res.naM_DOTUOI2, res.naM_DOTUOI3, res.naM_DOTUOI4, res.naM_DOTUOI5, res.naM_DOTUOI6, res.naM_DOTUOI7],
                                        backgroundColor: '#319243',
                                        label: 'Nam'
                                    },
                                    {
                                        data: [res.nU_DOTUOI1, res.nU_DOTUOI2, res.nU_DOTUOI3, res.nU_DOTUOI4, res.nU_DOTUOI5, res.nU_DOTUOI6, res.nU_DOTUOI7],
                                        backgroundColor: '#c2c2c2',
                                        label: 'Nữ'
                                    }
                                ]
                            },
                            options: {
                                scales: {
                                    xAxes: [{
                                        ticks: {
                                            beginAtZero: true
                                        }
                                    }]
                                }
                            }
                        });
                    }

                }
            });

    },
    Thongkenhansu: function (elm) {


        $.ajax('/report/Thongkenhansu',
            {
                type: 'GET',  // http method
                success: function (data, status, xhr) {   // success callback function
                    if (data !== null && data.results.length > 0) {
                        var strHtml  ='';
                        $.each(data.results, function (idx, itm) {
                            $template = $('#template-status');
                            $template.find('.lb-count').text(itm.soluong);
                            $template.find('.lb-name').text(itm.tentrangthai);
                            strHtml += '<div class="col-md-2">' + $($template).html() + '</div>';
                        })

                        $('#status-nhansu').prepend(strHtml);
                    }

                }
            });

    },
    Trinhdotinhoc: function (elm) {


        $.ajax('/report/Trinhdotinhoc',
            {
                type: 'GET',  // http method
                success: function (data, status, xhr) {   // success callback function
                    if (data !== null && data.results.length > 0) {
                        var lsLabel = [];  
                        var lsData = []; 

                        $.each(data.results, function (idx, itm) {
                            lsLabel.push(itm.trinhdo);
                            lsData.push(itm.sl);
                        })

                        var ctx = document.getElementById(elm).getContext('2d');
                        var barChart = new Chart(ctx, {
                            type: 'bar',
                            data: {
                                labels: lsLabel,
                                datasets: [{
                                    label: 'Trìnhh độ tin học',
                                    data: lsData,
                                    backgroundColor: "#c2c2c2"
                                }]
                            }
                        });
                    }

                }
            });

    },
    Thongkenhansu_Donvi: function (elm) {


        $.ajax('/report/Thongkenhansu_Donvi',
            {
                type: 'GET',  // http method
                success: function (data, status, xhr) {   // success callback function
                    if (data !== null && data.results.length > 0) {
                        var strHtml = '';
                        $.each(data.results, function (idx, itm) {
                            var item = '';
                            item += ' <div class="notice notice-gray statistic-item">';
                            item += '     <div class="row">';
                            item += '         <div class="col-md-7">';
                            item += '             <label class="statistic-label ">' + itm.tendonvi + '</label>';
                            item += '         </div>';
                            item += '         <div class="col-md-3">';
                            item += '             <strong class="statistic-number" title="Số lượng">' + itm.sl +'</strong><i class="fa fa-male" aria-hidden="true"></i>';
                            item += '         </div>';
                            item += '         <div class="col-md-2">';
                            item += '             <label class="statistic-percent c-green" title="Tỉ lệ"><i class="fa fa-caret-up"></i> ' + itm.per + '</label>';
                            item += '         </div>';
                            item += '     </div>';
                            item += ' </div>';

                            //var item = ' <tr>';
                            //item += '  <th scope="row">1</th>';
                            //item += '       <td>' + itm.tendonvi + '</td>';
                            //item += '       <td><label class="c-green">' + itm.sl + '</label> / <label class="c-red">' + itm.per + '</label></td>';
                            //item += '       <td><a href="khenthuong/detail/">Chi tiết</a></td>';
                            //item += '  </tr>';
                            strHtml += item
                        });
                        $('#thongke-donvi').html(strHtml);
                    }

                }
            });

    },
    Tilekhenthuong: function (elm) {


        $.ajax('/report/Tilekhenthuong',
            {
                type: 'GET',  // http method
                success: function (data, status, xhr) {   // success callback function
                    if (data !== null && data.results.length > 0) {
                        var res = data.results[0];
                        const percent = res.per;
                        const color = '#01713c';
                        const canvas = 'ti-le-khen-thuong-chart';
                        const container = 'ti-le-khen-thuong';

                        const percentValue = percent; // Sets the single percentage value
                        const colorGreen = color, // Sets the chart color
                            animationTime = '1400'; // Sets speed/duration of the animation

                        const chartCanvas = document.getElementById(canvas), // Sets canvas element by ID
                            chartContainer = document.getElementById(container), // Sets container element ID
                            divElement = document.createElement('div'), // Create element to hold and show percentage value in the center on the chart
                            domString = '<div class="chart__value"><p>' + percentValue + '%</p></div>'; // String holding markup for above created element

                        // Create a new Chart object
                        const doughnutChart = new Chart(chartCanvas, {
                            type: 'doughnut', // Set the chart to be a doughnut chart type
                            data: {
                                datasets: [
                                    {
                                        data: [percentValue, 100 - percentValue], // Set the value shown in the chart as a percentage (out of 100)
                                        backgroundColor: [colorGreen], // The background color of the filled chart
                                        borderWidth: 0 // Width of border around the chart
                                    }
                                ]
                            },
                            options: {
                                cutoutPercentage: 84, // The percentage of the middle cut out of the chart
                                responsive: false, // Set the chart to not be responsive
                                tooltips: {
                                    enabled: false // Hide tooltips
                                }
                            }
                        });

                        Chart.defaults.global.animation.duration = animationTime; // Set the animation duration

                        divElement.innerHTML = domString; // Parse the HTML set in the domString to the innerHTML of the divElement
                        chartContainer.appendChild(divElement.firstChild); // Append the divElement within the chartContainer as it's child
                    }

                }
            });

    },
    Tilekyluat: function (elm) {


        $.ajax('/report/Tilekyluat',
            {
                type: 'GET',  // http method
                success: function (data, status, xhr) {   // success callback function
                    if (data !== null && data.results.length > 0) {
                        const percent = 12;
                        const color = '#7c7c7c';
                        const canvas = 'ti-le-ky-luat-chart';
                        const container = 'ti-le-ky-luat';

                        const percentValue = percent; // Sets the single percentage value
                        const colorGreen = color, // Sets the chart color
                            animationTime = '1400'; // Sets speed/duration of the animation

                        const chartCanvas = document.getElementById(canvas), // Sets canvas element by ID
                            chartContainer = document.getElementById(container), // Sets container element ID
                            divElement = document.createElement('div'), // Create element to hold and show percentage value in the center on the chart
                            domString = '<div class="chart__value"><p>' + percentValue + '%</p></div>'; // String holding markup for above created element

                        // Create a new Chart object
                        const doughnutChart = new Chart(chartCanvas, {
                            type: 'doughnut', // Set the chart to be a doughnut chart type
                            data: {
                                datasets: [
                                    {
                                        data: [percentValue, 100 - percentValue], // Set the value shown in the chart as a percentage (out of 100)
                                        backgroundColor: [colorGreen], // The background color of the filled chart
                                        borderWidth: 0 // Width of border around the chart
                                    }
                                ]
                            },
                            options: {
                                cutoutPercentage: 84, // The percentage of the middle cut out of the chart
                                responsive: false, // Set the chart to not be responsive
                                tooltips: {
                                    enabled: false // Hide tooltips
                                }
                            }
                        });

                        Chart.defaults.global.animation.duration = animationTime; // Set the animation duration

                        divElement.innerHTML = domString; // Parse the HTML set in the domString to the innerHTML of the divElement
                        chartContainer.appendChild(divElement.firstChild); // Append the divElement within the chartContainer as it's child
                    }

                }
            });

    },
    Trinhdohocvan: function (elm) {


        $.ajax('/report/Trinhdohocvan',
            {
                type: 'GET',  // http method
                success: function (res, status, xhr) {   // success callback function
                    if (res !== null && res.results.length > 0) {
                        var lsLabel = [];
                        var lsData = [];

                        $.each(res.results, function (idx, itm) {
                            lsLabel.push(itm.tentdcm);
                            lsData.push(itm.sl);
                        });

                        var data = {
                            legend: {
                                title: "aaaa"
                            },
                            labels: lsLabel,
                            datasets: [{
                                data: lsData,
                                backgroundColor: [
                                    "#319243",
                                    "#7abc87",
                                    "#c2c2c2"
                                ],
                                hoverBackgroundColor: [
                                    "#0d4017",
                                    "#579162",
                                    "#959595"
                                ],
                                borderWidth: 1
                            }]
                        };

                        var options = {
                            animation: {
                                animateRotate: true,
                                animateScale: true
                            },
                            cutoutPercentage: 85,
                            legend: false,
                            legendCallback: function (chart) {
                                var text = [];
                                var number = 0;
                                text.push('<ul class="' + chart.id + '-legend list-unstyled">');
                                for (var i = 0; i < chart.data.datasets[0].data.length; i++) {
                                    text.push('<li><span style="background-color:' + chart.data.datasets[0].backgroundColor[i] + '"></span>');
                                    if (chart.data.labels[i]) {
                                        text.push(" ");
                                        text.push(chart.data.labels[i]);
                                        text.push(" - ");
                                        text.push(chart.data.datasets[0].data[i]);
                                        text.push(' <i class="fa fa-user" aria-hidden="true"></i>');
                                        number += chart.data.datasets[0].data[i];
                                    }
                                    text.push('</li>');
                                }
                                text.push('</ul>');
                                $("#chartjs-tooltip").html(number);
                                return text.join("");
                            },
                            tooltips: {
                                custom: function (tooltip) {
                                    //tooltip.x = 0;
                                    //tooltip.y = 0;
                                },
                                mode: 'single',
                                callbacks: {
                                    label: function (tooltipItems, data) {
                                        var sum = data.datasets[0].data.reduce(add, 0);
                                        function add(a, b) {
                                            return a + b;
                                        }

                                        return parseInt((data.datasets[0].data[tooltipItems.index] / sum * 100), 10) + ' %';
                                    },
                                    beforeLabel: function (tooltipItems, data) {
                                        return data.datasets[0].data[tooltipItems.index] + ' hrs';
                                    }
                                }
                            }
                        }
                        var ctx = $("#myChart");
                        var myChart = new Chart(ctx, {
                            type: 'doughnut',
                            data: data,
                            options: options
                        });
                        $("#chartjs-legend").html(myChart.generateLegend());
                    }

                }
            });

    },
    Trinhdongoaingu: function (elm) {


        $.ajax('/report/Trinhdongoaingu',
            {
                type: 'GET',  // http method
                success: function (res, status, xhr) {   // success callback function
                    if (res !== null && res.results.length > 0) {
                        var uniqueGioiTinh = [... new Set(res.results.map(data => data.gioitinh))];
                        var uniqueNgoaiNgu = [... new Set(res.results.map(data => data.tenn))];
                        var lsColors = ["rgba(200,0,0,0.2)", "#7abc87bf"];
                        var dataset = [];
                        for (var i = 0; i < uniqueGioiTinh.length; i++) {
                            var lsData = [];
                            for (var j = 0; j < uniqueNgoaiNgu.length; j++) {
                                var item = res.results.find(x => x.gioitinh === uniqueGioiTinh[i] && x.tenn === uniqueNgoaiNgu[j]);
                                var sl = item !== undefined ? item.sl : 0;
                                lsData.push(sl);
                            }
                            var _dtsitem = {
                                label: uniqueGioiTinh[i],
                                backgroundColor: lsColors[i],
                                data: lsData
                            }
                            dataset.push(_dtsitem);
                        }

                        var marksCanvas = document.getElementById("chart-do-tuoi-dang-vien");

                        var marksData = {
                            labels: uniqueNgoaiNgu,
                            datasets: dataset
                        };

                        var radarChart = new Chart(marksCanvas, {
                            type: 'radar',
                            data: marksData
                        });
                    }
                }
            });
        
    },
    Thongkecbns: function (elm) {


        $.ajax('/report/Thongkecbns',
            {
                type: 'GET',  // http method
                success: function (res, status, xhr) {   // success callback function
                    if (res !== null && res.results.length > 0) {
                        var uniqueGioiTinh = [... new Set(res.results.map(data => data.gioitinh))];
                        var uniqueThang = [... new Set(res.results.map(data => data.thang))];
                        var lsColors = ["#319243", "#ff3e3e"];
                        var dataset = [];
                        for (var i = 0; i < uniqueGioiTinh.length; i++) {
                            var lsData = [];
                            for (var j = 0; j < uniqueThang.length; j++) {
                                var item = res.results.find(x => x.gioitinh === uniqueGioiTinh[i] && x.thang === uniqueThang[j]);
                                var sl = item !== undefined ? item.sl : 0;
                                lsData.push(sl);
                            }
                            var _dtsitem = {
                                label: uniqueGioiTinh[i],
                                data: lsData,
                                fill: false,
                                borderColor: lsColors[i],
                                borderWidth: 1,
                                pointStyle: 'rectRot',
                                pointRadius: 5,
                                pointBorderColor: lsColors[i],
                                tension: 0.1
                            }
                            dataset.push(_dtsitem);
                        }


                        var chart_bien_chuyen_nhan_su = document.getElementById("chart-bien-chuyen-nhan-su");
                        var myLineChart = new Chart(chart_bien_chuyen_nhan_su, {
                            type: 'line',
                            data: {
                                labels: uniqueThang,
                                datasets: dataset
                            }
                        });
                    }
                }
            });

    },
    Donviktkl: function (elm) {


        $.ajax('/report/Donviktkl',
            {
                type: 'GET',  // http method
                success: function (data, status, xhr) {   // success callback function
                    if (data !== null && data.results.length > 0) {
                        var strHtml = '';
                        $.each(data.results, function (idx, itm) {
                       
                            var item = ' <tr>';
                            item += '  <th scope="row">' + (++idx)+'</th>';
                            item += '       <td>' + itm.tendonvi + '</td>';
                            item += '       <td><label class="c-green"><a href="report/DSktkl?MaDonVi=' + itm.madonvi + '&loai=0">' + itm.khenthuong + '</a></label> / <label><a class="c-red" href="report/DSktkl?MaDonVi=' + itm.madonvi + '&loai=1">' + itm.kyluat +'</a></label></td>';
                     
                            item += '  </tr>';
                            strHtml += item
                        });
                        $('#tb-donvi tbody').html(strHtml);
                    }
                }
            });

    },
    DSktkl: function (elm) {


        $.ajax('/report/DSktkl',
            {
                type: 'GET',  // http method
                success: function (res, status, xhr) {   // success callback function
                    if (res !== null && res.results.length > 0) {

                    }
                }
            });

    }
}


