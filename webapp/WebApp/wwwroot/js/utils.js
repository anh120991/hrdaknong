﻿'use strict';

window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};

window.label = {
	addsuccess: 'Thêm mới thành công'
};

window.notification = {
	error: function (mesage) {
		window.createNotification({
			closeOnClick: true,
			displayCloseButton: true,
			positionClass: 'nfc-bottom-right',
			showDuration: 2000,
			theme: 'error'
		})({
			title: 'Lỗi',
			message: mesage
		});
	},
	success: function (mesage) {
		window.createNotification({
			closeOnClick: true,
			displayCloseButton: true,
			positionClass: 'nfc-bottom-right',
			showDuration: 2000,
			theme: 'success'
		})({
			title: 'Thành công',
			message: mesage
		});
	},
	warning: function (mesage) {
		window.createNotification({
			closeOnClick: true,
			displayCloseButton: true,
			positionClass: 'nfc-bottom-right',
			showDuration: 2000,
			theme: 'Warning'
		})({
			title: 'dsada',
			message: 'dsadsadsa321'
		});
	},
};


   
(function (global) {
	var MONTHS = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];

	var COLORS = [
		'#4dc9f6',
		'#f67019',
		'#f53794',
		'#537bc4',
		'#acc236',
		'#166a8f',
		'#00a950',
		'#58595b',
		'#8549ba'
	];

	var Samples = global.Samples || (global.Samples = {});
	var Color = global.Color;

	Samples.utils = {
		// Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
		srand: function (seed) {
			this._seed = seed;
		},

		rand: function (min, max) {
			var seed = this._seed;
			min = min === undefined ? 0 : min;
			max = max === undefined ? 1 : max;
			this._seed = (seed * 9301 + 49297) % 233280;
			return min + (this._seed / 233280) * (max - min);
		},

		numbers: function (config) {
			var cfg = config || {};
			var min = cfg.min || 0;
			var max = cfg.max || 1;
			var from = cfg.from || [];
			var count = cfg.count || 8;
			var decimals = cfg.decimals || 8;
			var continuity = cfg.continuity || 1;
			var dfactor = Math.pow(10, decimals) || 0;
			var data = [];
			var i, value;

			for (i = 0; i < count; ++i) {
				value = (from[i] || 0) + this.rand(min, max);
				if (this.rand() <= continuity) {
					data.push(Math.round(dfactor * value) / dfactor);
				} else {
					data.push(null);
				}
			}

			return data;
		},

		labels: function (config) {
			var cfg = config || {};
			var min = cfg.min || 0;
			var max = cfg.max || 100;
			var count = cfg.count || 8;
			var step = (max - min) / count;
			var decimals = cfg.decimals || 8;
			var dfactor = Math.pow(10, decimals) || 0;
			var prefix = cfg.prefix || '';
			var values = [];
			var i;

			for (i = min; i < max; i += step) {
				values.push(prefix + Math.round(dfactor * i) / dfactor);
			}

			return values;
		},

		months: function (config) {
			var cfg = config || {};
			var count = cfg.count || 12;
			var section = cfg.section;
			var values = [];
			var i, value;

			for (i = 0; i < count; ++i) {
				value = MONTHS[Math.ceil(i) % 12];
				values.push(value.substring(0, section));
			}

			return values;
		},

		color: function (index) {
			return COLORS[index % COLORS.length];
		},

		transparentize: function (color, opacity) {
			var alpha = opacity === undefined ? 0.5 : 1 - opacity;
			return Color(color).alpha(alpha).rgbString();
		}
	};

	// DEPRECATED
	window.randomScalingFactor = function () {
		return Math.round(Samples.utils.rand(-100, 100));
	};

	// INITIALIZATION

	Samples.utils.srand(Date.now());

	// Google Analytics
	/* eslint-disable */
	if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
		(function (i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date(); a = s.createElement(o),
				m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
		ga('create', 'UA-28909194-3', 'auto');
		ga('send', 'pageview');
	}
	/* eslint-enable */
	Samples.utils.toDM = function (date) {
		var strDate = '';
		var _tempDate = new Date(date);
		var _d = _tempDate.getDate();
		var _m = _tempDate.getMonth() + 1;
		strDate = (_d < 10 ? '0' + _d : _d) + "/" + (_m < 10 ? '0' + _m : _m);
		return strDate;
    }
	Samples.utils.datediff = function (start, end) {
		// format dd/mm/yyyy
		var from = datetimepickertoDate(start);
		var to = datetimepickertoDate(end);


		var diff = new Date(to - from);

		// get days
		var days = diff / 1000 / 60 / 60 / 24;
		return days;
	}
	var datetimepickertoDate = function (date) {
		var resultDate;
		// format dd/mm/yyyy
		if (date.indexOf('/') > 1) {
			var attr = date.split('/');
			if (attr.length > 2) {
				resultDate = new Date(attr[2] + '/' + attr[1] + '/' + attr[0]);
            }
		}
		return resultDate;
	}
	

}(this));
