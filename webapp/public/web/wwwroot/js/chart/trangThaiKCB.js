﻿var BenhNhan = {
    Init: function (element) {

    },
    buildPieChartTSBN: function (element, date) {
        
        $.ajax('/chart/GetChartTongSoBenhNhan',
            {
                type: 'GET',  // http method
                data: { dateTime: date },
                success: function (data, status, xhr) {   // success callback function
                    var pieChartSoBN = new CanvasJS.Chart(element, {
                        //animationEnabled: true,
                        //data: data
                        exportEnabled: false,
                        animationEnabled: true,
                        title: {
                            text: "Tổng số bệnh nhân",
                            fontFamily: "tahoma"
                        },
                        legend: {
                            cursor: "pointer"
                        },
                        data: data
                    });
                    pieChartSoBN.render();
                }
            });

    },
    refreshPieChartTSBN: function () {

    },

    buildLineChart7NgaySoBNKB: function (element, date) {
        $.ajax('/chart/GetChartChiTietTongBenhNhanKCBTheoTuan',
            {
                type: 'GET',  // http method
                data: { dateTime: date },
                success: function (data, status, xhr) {   // success callback function
                    var _tempDate = [];
                    if (data !== null && data.length > 0) {
                        $.each(data, function (idx, itm) {
                            var item = {
                                y : itm.y,
                                x : new Date(itm.x)
                            }
                            _tempDate.push(item);
                        });
                    }
                    var lineChartSoBNLK = new CanvasJS.Chart(element, {
                        animationEnabled: true,
                        title: {
                            text: "Số lượng bệnh nhân 7 ngày trước",
                            fontFamily: "tahoma"
                        },
                        axisY: {

                            valueFormatString: "#",
                            suffix: "người"
                        },
                        data: [{
                            yValueFormatString: "# người",
                            xValueFormatString: "DD-MM",
                            type: "spline",
                            dataPoints: _tempDate
                        }]
                    });
                    lineChartSoBNLK.render();
                }
            });
    }
}