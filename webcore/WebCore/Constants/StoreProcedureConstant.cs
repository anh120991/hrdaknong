﻿namespace WebCore.Constants
{
    public class StoreProcedureConstant
    {
        #region MASTER DATA 
        public const string SP_PATIENT_SUBJECT = "PKG_REPORT.SP_PATIENT_SUBJECT";
        public const string SP_COME_HOSPITAL = "PKG_REPORT.SP_COME_HOSPITAL";
        public const string SP_WAIT_EXAMINE = "PKG_REPORT.SP_WAIT_EXAMINE";
        public const string SP_TIME_EXAMINE = "PKG_REPORT.SP_TIME_EXAMINE";
        public const string SP_TREATMENT = "PKG_REPORT.SP_TREATMENT";
        public const string SP_RESULT = "PKG_REPORT.SP_RESULT";
        #endregion

        public const string SP_TOTAL_HC_STATUS = "PKG_REPORT.SP_TOTAL_HC_STATUS";
        public const string SP_HEALTHCARE_STATUS = "PKG_REPORT.SP_HEALTHCARE_STATUS";
        public const string SP_SUB_DEPARTMENT = "PKG_REPORT.SP_SUB_DEPARTMENT";
        public const string SP_TOTAL_SUB_DEPARTMENT = "PKG_REPORT.SP_TOTAL_SUB_DEPARTMENT";
        public const string SP_DEPARTMENT_MAJORS = "PKG_REPORT.SP_DEPARTMENT_MAJORS";
        public const string SP_TOTAL_DEPARTMENT_MAJORS = "PKG_REPORT.SP_TOTAL_DEPARTMENT_MAJORS";
        public const string SP_TOTAL_PHARMA_SUB_DEPARTMENT = "PKG_REPORT.SP_TOTAL_PHARMA_SUB_DEPARTMENT";
        public const string SP_TOTAL_EM_PATIENT_SUBJECT = "PKG_REPORT.SP_TOTAL_EM_PATIENT_SUBJECT";
        public const string SP_TOTAL_EM_TREATMENT = "PKG_REPORT.SP_TOTAL_EM_TREATMENT";
        public const string SP_TOTAL_SURGERY = "PKG_REPORT.SP_TOTAL_SURGERY";
        public const string SP_TOTAL_SUR_PATIENT_SUBJECT = "PKG_REPORT.SP_TOTAL_SUR_PATIENT_SUBJECT";
        public const string SP_TOTAL_SUR_DEPARMENT = "PKG_REPORT.SP_TOTAL_SUR_DEPARMENT";
        public const string SP_TOTAL_SUR_GROUP = "PKG_REPORT.SP_TOTAL_SUR_GROUP";

        public const string SP_TOTAL_IP_P = "PKG_REPORT.SP_TOTAL_IP_P";
        public const string SP_TOTAL_IP_ADMIT_DEPT = "PKG_REPORT.SP_TOTAL_IP_ADMIT_DEPT";
        public const string SP_TOTAL_IP_OUT_DEPT = "PKG_REPORT.SP_TOTAL_IP_OUT_DEPT";
        public const string SP_TOTAL_IP_TREATMENT = "PKG_REPORT.SP_TOTAL_IP_TREATMENT";
        public const string SP_TOTAL_IP_RESULT = "PKG_REPORT.SP_TOTAL_IP_RESULT";
        public const string SP_TOTAL_IP_PATIENT_SUBJECT = "PKG_REPORT.SP_TOTAL_IP_PATIENT_SUBJECT";

        //

        public const string SP_THONGKENHANSU = "PKG_REPORT.SP_THONGKENHANSU";
        public const string SP_THONGKEDANGVIEN = "PKG_REPORT.SP_THONGKEDANGVIEN";
        public const string SP_DOTUOI = "PKG_REPORT.SP_DOTUOI";
        public const string SP_TRINHDOTINHOC = "PKG_REPORT.SP_TRINHDOTINHOC";
        public const string SP_THONGKENHANSU_DONVI = "PKG_REPORT.SP_THONGKENHANSU_DONVI";
        public const string SP_THONGKETYLEKHENTHUONG = "PKG_REPORT.SP_THONGKETYLEKHENTHUONG";
        public const string SP_THONGKETYLEKYLUAT = "PKG_REPORT.SP_THONGKETYLEKYLUAT";
        public const string SP_DONVI_KT_KL = "PKG_REPORT.SP_DONVI_KT_KL";
        public const string SP_THONGKE_TDHV = "PKG_REPORT.SP_THONGKE_TDHV";
        public const string SP_THONGKE_TDNN = "PKG_REPORT.SP_THONGKE_TDNN";
        public const string SP_THONGKE_CBNS = "PKG_REPORT.SP_THONGKE_CBNS";
        public const string SP_DM_DONVI = "PKG_REPORT.SP_DM_DONVI";
        public const string SP_DM_CHUCVU = "PKG_REPORT.SP_DM_CHUCVU";
        public const string SP_DM_TRANGTHAIHOSO = "PKG_REPORT.SP_DM_TRANGTHAIHOSO";
        public const string SP_DS_HOSO = "PKG_REPORT.SP_DS_HOSO";
        public const string SP_DS_KHENTHUONG_KYLUAT = "PKG_REPORT.SP_DS_KHENTHUONG_KYLUAT";



    }
}