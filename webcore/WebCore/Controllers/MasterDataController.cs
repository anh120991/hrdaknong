﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Services;
using WebCore.Services.Interfaces;
using WebCore.ViewModels;

namespace WebCore.Controllers
{
    [Route("api/masterdata")]
    [ApiController]
    public class MasterDataController : Controller
    {
        IMasterDataService _masterDataService;
        public MasterDataController(IMasterDataService masterDataService)
        {
            _masterDataService = masterDataService;
        }

        [HttpGet("v1.0.0/dm-donvi")]
        public async Task<GenericResult<DMDVResponseModel>> DM_DonVi()
        {
            return await _masterDataService.DM_DonVi();
        }

        [HttpGet("v1.0.0/dm-chucvu")]
        public async Task<GenericResult<PositionResponseModel>> DM_Chucvu()
        {
            return await _masterDataService.DM_Chucvu();
        }
        [HttpGet("v1.0.0/dm-trangthaihoso")]
        public async Task<GenericResult<StatusResponseModel>> DM_TSHS()
        {
            return await _masterDataService.DM_Trangthaihoso();
        }

        [HttpPost("v1.0.0/dshs")]
        public async Task<GenericResult<HSResponseModel>> DS_HS(DSHSRequestModel model)
        {
            return await _masterDataService.DS_HS(model);
        }
    }
}
