﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Services.Interfaces;
using WebCore.ViewModels.Menu;

namespace WebCore.Controllers
{
    [Route("api/menu")]
    [ApiController]
    public class MenuController : Controller
    {
        private IMenuService _menuService;

        public MenuController(IMenuService menuService)
        {
            _menuService = menuService;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet("v1.0.0/get-menu")]
        public async Task<GenericResult<MenuResponseModel>> GetMenu()
        {
            var response = await _menuService.GetMenuAsync();
            return response;
        }
        [HttpPost("v1.0.0/add-new-menu")]
        public async Task<GenericResult<bool>> CreateRoleAsync([FromBody] MenuRequestModel model)
        {
            var response = await _menuService.CreateNewMenuAsync(model);
            return response;
        }

        [HttpPost("v1.0.0/update-menu")]
        public async Task<GenericResult<bool>> UpdateRoleAsync([FromBody] MenuRequestModel model)
        {
            var response = await _menuService.UpdateMenuAsync(model);
            return response;
        }

        [HttpGet("v1.0.0/get-menu-detail/{id}")]
        public async Task<GenericResult<MenuResponseModel>> UpdateRoleAsync([FromRoute] long id)
        {
            var response = await _menuService.GetDetailMenuAsync(id);
            return response;
        }
    }
}
