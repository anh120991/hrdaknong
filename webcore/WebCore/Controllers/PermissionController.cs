﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Services.Interfaces;
using WebCore.ViewModels.Permission;

namespace WebCore.Controllers
{
    [Route("api/permissions")]
    [Authorize]
    public class PermissionController : Controller
    {
        private readonly IPermissionService _permission;

        public PermissionController(IPermissionService permission)
        {
            _permission = permission;
        }


        [HttpGet("v1.0.0/get-all-permission")]
        public async Task<GenericResult<PermissionResponseModel>> GetAllPermissionAsync()
        {
            var response = await _permission.GetAllPermissionAsync();
            return response;
        }

        [HttpPost("v1.0.0/add-new-permission")]
        public async Task<GenericResult<bool>> CreatePermissionAsync([FromBody]PermissionRequestModel model)
        {
            var response = await _permission.CreatePermissionAsync(model);
            return response;
        }

        [HttpPost("v1.0.0/update-permission")]
        public async Task<GenericResult<bool>> UpdatePermissionAsync([FromBody]PermissionRequestModel model)
        {
            var response = await _permission.UpdatePermissionAsync(model);
            return response;
        }

        [HttpGet("v1.0.0/get-permission-detail/{id}")]
        public async Task<GenericResult<PermissionResponseModel>> UpdateRoleAsync([FromRoute] long id)
        {
            var response = await _permission.GetDetailPermissionAsync(id);
            return response;
        }
    }
}