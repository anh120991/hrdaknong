﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Services.Interfaces;
using WebCore.ViewModels;

namespace WebCore.Controllers
{
    [Route("api/report")]
    [Authorize]
    public class ReportController : Controller
    {
        private readonly IReportService _report;

        public ReportController(IReportService report)
        {
            _report = report;
        }


        [HttpGet("v1.0.0/thongkedangvien")]
        public async Task<GenericResult<DangvienResponseModel>> Thongkedangvien()
        {
            return await _report.Thongkedangvien();
        }
        [HttpGet("v1.0.0/dotuoi")]
        public async Task<GenericResult<TuoiResponseModel>> Dotuoi()
        {
            return await _report.Dotuoi();
        }
        [HttpGet("v1.0.0/thongkenhansu")]
        public async Task<GenericResult<TKNhansuResponseModel>> Thongkenhansu()
        {
            return await _report.Thongkenhansu();
        }
        [HttpGet("v1.0.0/thongkenhansu-donvi")]
        public async Task<GenericResult<NhansuResponseModel>> Thongkenhansu_Donvi()
        {
            return await _report.Thongkenhansu_Donvi();
        }
        [HttpGet("v1.0.0/trinhdotinhoc")]
        public async Task<GenericResult<TinhocResponseModel>> Trinhdotinhoc()
        {
            return await _report.Trinhdotinhoc();
        }
        [HttpGet("v1.0.0/tilekhenthuong")]
        public async Task<GenericResult<TileResponseModel>> Thongketylekhenthuong()
        {
            return await _report.Thongketylekhenthuong();
        }
        [HttpGet("v1.0.0/tilekyluat")]
        public async Task<GenericResult<TileResponseModel>> Thongketylekyluat()
        {
            return await _report.Thongketylekyluat();
        }

        [HttpGet("v1.0.0/trinhdohocvan")]
        public async Task<GenericResult<TDHVResponseModel>> Thongke_tdhv()
        {
            return await _report.Thongke_tdhv();
        }
        [HttpGet("v1.0.0/trinhdongoaingu")]
        public async Task<GenericResult<TDNNResponseModel>> Thongke_tdnn()
        {
            return await _report.Thongke_tdnn();
        }

        [HttpGet("v1.0.0/thongke-cbns")]
        public async Task<GenericResult<CBNSResponseModel>> Thongke_cbns()
        {
            return await _report.Thongke_cbns();
        }

        [HttpGet("v1.0.0/donvi-ktkl")]
        public async Task<GenericResult<DVKTKLResponseModel>> Donvi_ktkl()
        {
            return await _report.Donvi_ktkl();
        }
        [HttpPost("v1.0.0/ds-ktkl")]
        public async Task<GenericResult<DSKTKLResponseModel>> DS_khenthuong_kyluat([FromBody] DVKTKLRequestModel model)
        {
            return await _report.DS_khenthuong_kyluat(model);
        }
    }
}