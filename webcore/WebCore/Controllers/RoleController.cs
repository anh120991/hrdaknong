﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Services.Interfaces;
using WebCore.ViewModels.Role;

namespace WebCore.Controllers
{
    [Route("api/role")]
    [Authorize]
    public class RoleController : Controller
    {
        private readonly IRoleService _role;

        public RoleController(IRoleService role)
        {
            _role = role;
        }

        [HttpGet("v1.0.0/get-all-role")]
        public async Task<GenericResult<RoleResponseModel>> GetAllRoleAsync()
        {
            var response = await _role.GetAllRoleAsync();
            return response;
        }

        [HttpPost("v1.0.0/add-new-role")]
        public async Task<GenericResult<bool>> CreateRoleAsync([FromBody]RoleRequestModel model)
        {
            var response = await _role.CreateNewRoleAsync(model);
            return response;
        }

        [HttpPost("v1.0.0/update-role")]
        public async Task<GenericResult<bool>> UpdateRoleAsync([FromBody]RoleRequestModel model)
        {
            var response = await _role.UpdateRoleAsync(model);
            return response;
        }

        [HttpGet("v1.0.0/get-role-detail/{id}")]
        public async Task<GenericResult<RoleResponseModel>> UpdateRoleAsync([FromRoute] long id)
        {
            var response = await _role.GetDetailRoleAsync(id);
            return response;
        }
    }
}