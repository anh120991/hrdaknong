﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebCore.Constants;
using WebCore.Infrastructure.Attribute;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Services.Interfaces;
using WebCore.ViewModels.Treatment;

namespace WebCore.Controllers
{
    [Route("api/treatment")]
    [Authorize]
    public class TreatmentController : Controller
    {
        private readonly ITreatmentService _treatment;

        public TreatmentController(ITreatmentService treatment)
        {
            _treatment = treatment;
        }

        [Permission(PermissionConstant.PermissionKeyA)]
        [HttpPost("v1.0.0/get-total-ip-patient")]
        public async Task<GenericResult<TotalIpPatientsResponseModel>> GetTotalIpPatientsAsync([FromBody]TreatmentRequestModel model)
        {
            return await _treatment.GetTotalIpPatientsAsync(model);
        }

        [HttpPost("v1.0.0/get-total-ip-admit-dept")]
        public async Task<GenericResult<TotalIpAdmitDeptResponseModel>> GetTotalIpAdmitPatientsAsync([FromBody]TreatmentRequestModel model)
        {
            return await _treatment.GetTotalIpAdmitPatientsAsync(model);
        }

        [HttpPost("v1.0.0/get-total-out-dept")]
        public async Task<GenericResult<TotalIpOutDeptResponseModel>> GetTotalIpOutDeptAsync([FromBody]TreatmentRequestModel model)
        {
            return await _treatment.GetTotalIpOutDeptAsync(model);
        }

        [HttpPost("v1.0.0/get-total-ip-treatment")]
        public async Task<GenericResult<TotalIpTreatmentResponseModel>> GetTotalIpTreatmentAsync([FromBody]TreatmentRequestModel model)
        {
            return await _treatment.GetTotalIpTreatmentAsync(model);
        }

        [HttpPost("v1.0.0/get-total-ip-result")]
        public async Task<GenericResult<TotalIpResultResponseModel>> GetTotalIpResultAsync([FromBody]TreatmentRequestModel model)
        {
            return await _treatment.GetTotalIpResultAsync(model);
        }

        [HttpPost("v1.0.0/get-total-ip-patient-subject")]
        public async Task<GenericResult<TotalIpPatientSubjectResponseModel>> GetTotalIpPatientSubjectAsync([FromBody]TreatmentRequestModel model)
        {
            return await _treatment.GetTotalIpPatientSubjectAsync(model);
        }
    }
}