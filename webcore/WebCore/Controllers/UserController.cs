﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Services.Interfaces;
using WebCore.ViewModels.User;

namespace WebCore.Controllers
{
    [Route("api/users")]
    [Authorize]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly IReportService _report;

        public UserController(IUserService userService, IReportService report)
        {
            _userService = userService;
            _report = report;
        }


        [HttpPost("v1.0.0/create-user")]
        public async Task<GenericResult<bool>> CreateNewUserAsync([FromBody]UserRequestModel model)
        {
            return await _userService.CreateUserAsync(model);
        }


        [HttpGet("v1.0.0/get-all-user")]
        public async Task<GenericResult<UserResponseModel>> GetAllUserAsync()
        {
            return await _userService.GetAllUser();
        }

        [HttpGet("v1.0.0/get-detail-user/{id}")]
        public async Task<GenericResult<UserResponseModel>> GetDetailUserAsync([FromRoute] long id)
        {
            return await _userService.GetDetailUser(id);
        }


        [HttpPost("v1.0.0/update-user")]
        public async Task<GenericResult<bool>> UpdateUserAsync([FromBody]UserRequestModel model)
        {
            return await _userService.UpdateUserAsync(model);
        }

        [HttpPost("v1.0.0/change-password")]
        public async Task<GenericResult<bool>> ChangePasswordAsync([FromBody]UserChangePasswordRequestModel model)
        {
            return await _userService.ChangePasswordAsync(model);
        }

        [HttpGet("v1.0.0/get-all-user-group")]
        public async Task<GenericResult<UserGroupResponseModel>> GetAllUserGroupAsync()
        {
            return await _userService.GetAllUserGroup();
        }



    }
}