﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace WebCore.Infrastructure.Attribute
{
    public class PermissionAttribute : AuthorizeAttribute
    {
        public PermissionAttribute()
            : base()
        {
        }

        public PermissionAttribute(params string[] roles)
            : base()
        {
            Roles = string.Join(",", roles);
        }
    }
}
