﻿using Microsoft.EntityFrameworkCore;
using WebCore.Models.Table;

namespace WebCore.Models
{
    public class DwhContext : DbContext
    {
        public DwhContext(DbContextOptions<DwhContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Group> Groups { get; set; }

        public DbSet<Menu> Menus { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {

                entity.Property(e => e.USERNAME)
                    .IsRequired()
                    .HasMaxLength(512);


                entity.HasMany(o => o.Profiles)
                    .WithOne(p => p.USER)
                    .HasForeignKey(d => d.USERID)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<UserRole>().HasKey(sc => new { sc.ROLEID, sc.USERID });
            modelBuilder.Entity<RolePermissions>().HasKey(sc => new { sc.ROLEID, sc.PERMISSIONID });
            modelBuilder.Entity<UserDepartment>().HasKey(sc => new { sc.DEPARTMENTID, sc.USERID });
            modelBuilder.Entity<Department>().HasKey(sc => sc.MADONVI);
            modelBuilder.Entity<Group>().HasKey(sc => sc.GROUPID);
            modelBuilder.Entity<UserGroup>().HasKey(sc => sc.GROUPID);
            modelBuilder.Entity<Menu>().HasKey(sc => sc.MENU_ID);

            modelBuilder.Entity<UserRole>()
                .HasOne(bc => bc.User)
                .WithMany(b => b.UserRoles)
                .HasForeignKey(bc => bc.USERID);

            modelBuilder.Entity<UserRole>()
                .HasOne(bc => bc.Role)
                .WithMany(c => c.UserRoles)
                .HasForeignKey(bc => bc.ROLEID);

            modelBuilder.Entity<UserDepartment>()
                .HasOne(bc => bc.Department)
                .WithMany(c => c.UserDepartments)
                .HasForeignKey(bc => bc.DEPARTMENTID);

            modelBuilder.Entity<UserDepartment>()
                .HasOne(bc => bc.User)
                .WithMany(c => c.UserDepartments)
                .HasForeignKey(bc => bc.USERID);

            modelBuilder.Entity<RolePermissions>()
                .HasOne(bc => bc.Role)
                .WithMany(b => b.RolePermissions)
                .HasForeignKey(bc => bc.ROLEID);

            modelBuilder.Entity<RolePermissions>()
                .HasOne(bc => bc.Permission)
                .WithMany(c => c.RolePermissions)
                .HasForeignKey(bc => bc.PERMISSIONID);

            modelBuilder.Entity<UserGroup>()
               .HasOne(bc => bc.Groups)
               .WithMany(b => b.UserGroups)
               .HasForeignKey(bc => bc.GROUPID);

            modelBuilder.Entity<UserGroup>()
                .HasOne(bc => bc.User)
                .WithMany(c => c.UserGroups)
                .HasForeignKey(bc => bc.USERID);
        }
    }
}
