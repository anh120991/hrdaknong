﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebCore.Models.Table
{
    [Table("DM_DONVI", Schema = "CBCC")]
    public class Department
    {
        public long MADONVI { get; set; }
        public string TENDONVI { get; set; }
        public long LOAI { get; set; }
        public string MA { get; set; }
        public int TRENCAP { get; set; }
       
        public virtual ICollection<UserDepartment> UserDepartments { get; set; } = new List<UserDepartment>();
    }
}