﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebCore.Models.Table
{
    [Table("SYS_GROUP_USER", Schema = "CBCC")]
    public class Group
    {
        public int GROUPID { get; set; }
        public string GROUPNAME { get; set; }
        public int ISMULTILDEPARTMENTS { get; set; }
        public virtual ICollection<UserGroup> UserGroups { get; set; } = new List<UserGroup>();
    }
}