﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebCore.Models.Table
{
    [Table("SYS_MENU", Schema = "CBCC")]
    public class Menu
    {
        public long MENU_ID { get; set; }
        [MaxLength(450)]
        public string GUID { get; set; }
        public string MENU_NAME { get; set; }
        public long MENU_PARENT_ID { get; set; }
        public int ISACTIVE { get; set; }
        public string MENU_DESCRIPTION { get; set; }
        public int ORDERBY { get; set; }
        public int PERMISSION_ID { get; set; }
        public string PATH { get; set; }
        public string CREATEDUSER { get; set; }
        public DateTime? CREATEDDATE { get; set; }
        public string UPDATEDUSER { get; set; }
        public DateTime? UPDATEDDATE { get; set; }
    }
}
