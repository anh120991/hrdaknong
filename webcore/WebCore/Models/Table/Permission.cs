﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebCore.Models.Table
{
    [Table("PERMISSION", Schema = "CBCC")]
    public class Permission
    {
        public long ID { get; set; }
        public string NAME { get; set; }
        public string CODE { get; set; }
        public long PARENTID { get; set; }
        public string DESCRIPTION { get; set; }
        public long DISPLAYORDER { get; set; }
        public int ISACTIVE { get; set; }

        public virtual ICollection<RolePermissions> RolePermissions { get; set; } = new List<RolePermissions>();
    }
}