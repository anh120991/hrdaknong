﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebCore.Models.Table
{
    [Table("PROFILE", Schema = "CBCC")]
    public class Profile
    {
        public long ID { get; set; }
        public long USERID { get; set; }
        public string FULLNAME { get; set; }
        public string EMAIL { get; set; }
        public string AVATAR { get; set; }
        public int GENDER { get; set; }
        public string ADDRESS { get; set; }
        public long DEPARTMENTID { get; set; }
        public virtual User USER { get; set; }
    }
}