﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebCore.Models.Table
{
    [Table("ROLE", Schema = "CBCC")]
    public class Role
    {
        public long ID { get; set; }
        [MaxLength(450)]
        public string GUID { get; set; }

        [MaxLength(250)]
        public string NAME { get; set; }

        public string DESCRIPTION { get; set; }

        public long? CREATEDBY { get; set; }

        public DateTime? CREATEDDATETIME { get; set; }

        public int ISACTIVE { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; } = new List<UserRole>();
        public virtual ICollection<RolePermissions> RolePermissions { get; set; } = new List<RolePermissions>();
    }
}
