﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebCore.Models.Table
{
    [Table("ROLEPERMISSIONS", Schema = "CBCC")]
    public class RolePermissions
    {
        public long ROLEID { get; set; }
        public Role Role { get; set; }
        public long PERMISSIONID { get; set; }
        public Permission Permission { get; set; }
        public int ENABLED { get; set; }
    }
}
