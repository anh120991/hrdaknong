﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebCore.Models.Table
{
    [Table("USERS", Schema="CBCC")]
    public class User
    {
        public long ID { get; set; }
        public string USERNAME { get; set; }
        public string PASSWORDHASH { get; set; }
        public string SALT { get; set; }
        public int ISACTIVE { get; set; }
        public int ISLOCKOUT { get; set; }
        public DateTime? LOCKOUTEND { get; set; }
        public int DELETED { get; set; }

        public DateTime? CREATEDDATE { get; set; }
        public DateTime? MODIFIEDDATE { get; set; }

        public virtual ICollection<Profile> Profiles { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
        public virtual ICollection<UserDepartment> UserDepartments { get; set; }
        public virtual ICollection<UserGroup> UserGroups { get; set; }
    }
}