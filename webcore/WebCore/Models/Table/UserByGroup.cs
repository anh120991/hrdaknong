﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebCore.Models.Table
{
    [Table("USERANDGROUP", Schema = "CBCC")]
    public class UserByGroup
    {
        public int GROUPID { get; set; }
        public int USERID { get; set; }


    }
}