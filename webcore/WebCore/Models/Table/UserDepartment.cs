﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebCore.Models.Table
{
    [Table("USERDEPARTMENT", Schema = "CBCC")]
    public class UserDepartment
    {
        public long USERID { get; set; }
        public User User { get; set; }

        public long DEPARTMENTID { get; set; }
        public Department Department { get; set; }
    }
}