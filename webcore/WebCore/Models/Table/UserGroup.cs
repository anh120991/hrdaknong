﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebCore.Models.Table
{
    [Table("USERGROUPS", Schema = "CBCC")]
    public class UserGroup
    {
        public int GROUPID { get; set; }
        public Group Groups { get; set; }
        public long USERID { get; set; }
        public User User { get; set; }
    }
}