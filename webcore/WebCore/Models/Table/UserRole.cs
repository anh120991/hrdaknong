﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebCore.Models.Table
{
    [Table("USERROLES", Schema = "CBCC")]
    public class UserRole
    {
        public long USERID { get; set; }
        public User User { get; set; }
        public long ROLEID { get; set; }
        public Role Role { get; set; }
    }
}