﻿using System.Threading.Tasks;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.ViewModels;
using WebCore.ViewModels.Chart;

namespace WebCore.Services
{
    public interface IChartService
    {
        Task<GenericResult<OverviewViewModel>> GetChartOverview(OverviewRequestModel model);
    }
}