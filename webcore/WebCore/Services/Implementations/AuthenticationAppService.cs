﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using WebCore.Constants;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Infrastructure.Extension;
using WebCore.Infrastructure.Helper;
using WebCore.Models;
using WebCore.Models.Table;
using WebCore.Services.Interfaces;
using WebCore.ViewModels;

namespace WebCore.Services.Implementations
{
    public class AuthenticationAppService : IAuthenticationAppService
    {
        private readonly TokenOptions _tokenOptions;
        private readonly DwhContext _dwhContext;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public AuthenticationAppService(IOptions<TokenOptions> tokenOptions, DwhContext dwhContext, IHttpContextAccessor httpContextAccessor)
        {
            _dwhContext = dwhContext;
            _httpContextAccessor = httpContextAccessor;
            _tokenOptions = tokenOptions.Value;
        }

        public async Task<GenericResult<LoginResponseModel>> AuthenticateWithUserNamePassword(LoginRequestModel model)
        {
            try
            {
                if (model == null)
                {
                    return GenericResult<LoginResponseModel>.Fail("Request is invalid.");
                }

                var user = _dwhContext.Users.Include(s => s.UserRoles).ThenInclude(s => s.Role).ThenInclude(s => s.RolePermissions).ThenInclude(s => s.Permission).FirstOrDefault(s => s.USERNAME == model.Username);
                if (user == null)
                {
                    return GenericResult<LoginResponseModel>.Fail("Username or password incorrect");
                }


                var pwdHash = CryptoHelpers.SecureHash(model.Password, user.SALT);

                if (pwdHash != user.PASSWORDHASH)
                {
                    return GenericResult<LoginResponseModel>.Fail("Username or password incorrect");
                }

                var permission = user.UserRoles.Select(s => s.Role).SelectMany(s => s.RolePermissions)
                    .Select(s => s.Permission).Select(s => s.CODE).Distinct().ToList();

                var token = GenerateNewTokens(user, permission, DateTime.UtcNow.AddMinutes(_tokenOptions.RefreshExpiration));

                return GenericResult<LoginResponseModel>.Succeed(token);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<GenericResult<LoginResponseModel>> AuthenticateWithRefreshToken(RefreshTokenRequestModel model)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var validationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_tokenOptions.RefreshSecret)),
                ValidateIssuer = true,
                ValidIssuer = _tokenOptions.Issuer,
                ValidateAudience = true,
                ValidAudience = _tokenOptions.Audience,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero,
                RequireExpirationTime = true,
            };

            SecurityToken validatedToken = null;
            try
            {
                IPrincipal principal = tokenHandler.ValidateToken(model.RefreshToken, validationParameters, out validatedToken);
            }
            catch
            {
                return GenericResult<LoginResponseModel>.Fail(HttpStatusCode.Unauthorized);
            }

            var refreshToken = validatedToken as JwtSecurityToken;
            var userIdFromRefreshToken = refreshToken?.Claims?.FirstOrDefault(r => r.Type == ClaimTypes.NameIdentifier)?.Value;
            if (string.IsNullOrEmpty(userIdFromRefreshToken))
            {
                return GenericResult<LoginResponseModel>.Fail(HttpStatusCode.Unauthorized);
            }

            var accessToken = tokenHandler.ReadJwtToken(model.AccessToken);
            var userIdFromAccessToken = accessToken?.Claims.FirstOrDefault(r => r.Type == ClaimTypes.NameIdentifier)?.Value;
            if (string.IsNullOrEmpty(userIdFromAccessToken))
            {
                return GenericResult<LoginResponseModel>.Fail(HttpStatusCode.Unauthorized);
            }

            if (userIdFromAccessToken != userIdFromRefreshToken)
            {
                return GenericResult<LoginResponseModel>.Fail(HttpStatusCode.Unauthorized);
            }

            var userId = long.Parse(userIdFromAccessToken);
            try
            {
                var test = _dwhContext.Users.FirstOrDefault(s => s.ID == userId);
            }
            catch (Exception ex)
            {

                throw;
            }
            var user = _dwhContext.Users.FirstOrDefault(s => s.ID == userId);
            if (user == null || user.ISACTIVE == 0)
            {
                return GenericResult<LoginResponseModel>.Fail(HttpStatusCode.Unauthorized);
            }

            var token = GenerateNewTokens(user, new List<string>(), refreshToken.ValidTo);
            return GenericResult<LoginResponseModel>.Succeed(token);
        }

        public async Task<GenericResult<LoggedUserDetailResponseModel>> GetCurrentUserDetailAsync()
        {
            try
            {
                long? userId = _httpContextAccessor.HttpContext.User.GetUserId();
                var user = await _dwhContext.Profiles.FirstOrDefaultAsync(s => s.USERID == userId);

                if (user == null)
                {
                    return GenericResult<LoggedUserDetailResponseModel>.Fail("User Does Not Exist");
                }

                var userModel = new LoggedUserDetailResponseModel
                {
                    Id = userId.ToString(),
                    Avatar = user.AVATAR,
                    FullName = user.FULLNAME,
                };

                return GenericResult<LoggedUserDetailResponseModel>.Succeed(userModel);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<GenericResult<bool>> Logout(string token)
        {
            var result = await LogoutToken(token);
            return GenericResult<bool>.Succeed(result);
        }

        private async Task<bool> LogoutToken(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var stringToken = tokenHandler.ReadJwtToken(token);
                var tokenRemainTime = stringToken.ValidTo - DateTime.UtcNow;
                if (tokenRemainTime.TotalMilliseconds > 0)
                {
                    return true;
                }

                return false;
            }
            catch (ArgumentException ex)
            {
                return false;
            }
        }

        private LoginResponseModel GenerateNewTokens(
            User user,
            IEnumerable<string> userPermissions,
            DateTime refreshExpiration,
            string dataPrivacyCacheKey = "")
        {
            var returnOutput = GenerateAccessTokens(user, userPermissions, dataPrivacyCacheKey);
            returnOutput.RefreshToken = GenerateRefreshTokens(user, refreshExpiration);
            return returnOutput;
        }

        private LoginResponseModel GenerateAccessTokens(
            User user,
            IEnumerable<string> userPermissions,
            string dataPrivacyCacheKey = "")
        {
            return GenerateAccessTokens(user, userPermissions, TimeSpan.FromMinutes(_tokenOptions.AccessExpiration), dataPrivacyCacheKey);
        }

        private LoginResponseModel GenerateAccessTokens(
            User user,
            IEnumerable<string> userPermissions,
            TimeSpan liveTime,
            string positionDataPrivacyCacheKey = "")
        {
            var now = DateTime.UtcNow;
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.USERNAME),
                new Claim(ClaimTypes.NameIdentifier, user.ID.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, now.ToString(CultureInfo.InvariantCulture), ClaimValueTypes.Integer64),
                new Claim(CustomClaimType.DataPrivacyCacheKey, positionDataPrivacyCacheKey),
            };

            if (userPermissions != null)
            {
                claims.AddRange(userPermissions.Select(r => new Claim(ClaimTypes.Role, r)));
            }

            var expires = now.Add(liveTime);
            var jwtToken = new JwtSecurityToken(
                _tokenOptions.Issuer,
                _tokenOptions.Audience,
                claims,
                expires: expires,
                signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_tokenOptions.Secret)),
                    SecurityAlgorithms.HmacSha512));

            var handler = new JwtSecurityTokenHandler();
            return new LoginResponseModel
            {
                Token = handler.WriteToken(jwtToken),
                ExpiresIn = expires
            };
        }

        private string GenerateRefreshTokens(User user, DateTime refreshExpiration)
        {
            var now = DateTime.UtcNow;
            var claim = new[]
            {
                new Claim(ClaimTypes.Email, user.USERNAME),
                new Claim(ClaimTypes.NameIdentifier, user.ID.ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, now.ToString(CultureInfo.InvariantCulture), ClaimValueTypes.Integer64),
            };

            var jwtToken = new JwtSecurityToken(
                _tokenOptions.Issuer,
                _tokenOptions.Audience,
                claim,
                expires: refreshExpiration,
                signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_tokenOptions.RefreshSecret)),
                    SecurityAlgorithms.HmacSha512));

            var handler = new JwtSecurityTokenHandler();
            return handler.WriteToken(jwtToken);
        }
    }
}