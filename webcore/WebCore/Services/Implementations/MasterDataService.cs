﻿using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;
using WebCore.Constants;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Infrastructure.Extension;
using WebCore.Models;
using WebCore.Models.StoreExtension;
using WebCore.Models.Table;
using WebCore.Services.Interfaces;
using WebCore.ViewModels;
using WebCore.ViewModels.Permission;

namespace WebCore.Services.Implementations
{
    public class MasterDataService : IMasterDataService
    {
        private readonly DwhContext _dbContext;

        public MasterDataService(DwhContext dwhContext)
        {
        }

        public async Task<GenericResult<PositionResponseModel>> DM_Chucvu()
        {
            var parameters = new OracleParameter[1];
            parameters[0] = new OracleParameter("v_out", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };
            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_DM_CHUCVU, parameters);
            var result = dt.ConvertToList<PositionResponseModel>();
            return GenericResult<PositionResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<DMDVResponseModel>> DM_DonVi()
        {
            var parameters = new OracleParameter[1];
            parameters[0] = new OracleParameter("v_out", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };
            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_DM_DONVI, parameters);
            var result = dt.ConvertToList<DMDVResponseModel>();
            return GenericResult<DMDVResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<StatusResponseModel>> DM_Trangthaihoso()
        {
            var parameters = new OracleParameter[1];
            parameters[0] = new OracleParameter("v_out", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };
            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_DM_TRANGTHAIHOSO, parameters);
            var result = dt.ConvertToList<StatusResponseModel>();
            return GenericResult<StatusResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<HSResponseModel>> DS_HS(DSHSRequestModel model)
        {
            var parameters = new OracleParameter[6];
            int i = 0;
            parameters[i++] = new OracleParameter("V_FRM_DATE", model.fromDate);
            parameters[i++] = new OracleParameter("V_TO_DATE", model.toDate);
            parameters[i++] = new OracleParameter("V_DONVI", model.donVi);
            parameters[i++] = new OracleParameter("V_CHUCVU", model.chucVu);
            parameters[i++] = new OracleParameter("V_TRANGTHAI", model.trangThai);
            parameters[i++] = new OracleParameter("v_out", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };
            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_DS_HOSO, parameters);
            var result = dt.ConvertToList<HSResponseModel>();
            return GenericResult<HSResponseModel>.Succeed(result);
        }
    }
}