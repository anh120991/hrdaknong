﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebCore.Constants;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Models;
using WebCore.Models.Table;
using WebCore.Services.Interfaces;
using WebCore.ViewModels.Menu;
using WebCore.ViewModels.Permission;

namespace WebCore.Services.Implementations
{
    public class MenuService : IMenuService
    {
        private readonly DwhContext _dbContext;

        public MenuService(DwhContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<GenericResult<bool>> CreateNewMenuAsync(MenuRequestModel model)
        {
            if (model == null) return GenericResult<bool>.Fail(SystemMessageConstant.RequestInvalid);

            var menu = new Menu
            {
                MENU_NAME = model.MenuName,
                ISACTIVE = model.IsActived,
                MENU_DESCRIPTION = model.MenuDescription,
                MENU_PARENT_ID = model.MenuParentId,
                ORDERBY = model.OrderBy,
                PATH = model.Path,
                PERMISSION_ID = model.PermissionId,
                GUID = Guid.NewGuid().ToString(),
                CREATEDDATE = DateTime.Now,
                CREATEDUSER = model.UserId,
                UPDATEDDATE = DateTime.Now,
                UPDATEDUSER = model.UserId
            };
            _dbContext.Add(menu);
            await _dbContext.SaveChangesAsync();

            return GenericResult<bool>.Succeeded;
        }

        public async Task<GenericResult<MenuResponseModel>> GetDetailMenuAsync(long id)
        {
            if (id == 0) return GenericResult<MenuResponseModel>.Failed;
            var menu = await _dbContext.Menus.FirstOrDefaultAsync(s => s.MENU_ID == id);
            if (menu == null) return GenericResult<MenuResponseModel>.Failed;

            var result = new MenuResponseModel
            {
                MENU_NAME = menu.MENU_NAME,
                ISACTIVED = menu.ISACTIVE,
                MENU_ID = menu.MENU_ID,
                Guid = menu.GUID,
                MENU_DESCRIPTION = menu.MENU_DESCRIPTION,
                MENU_PARENT_ID = menu.MENU_PARENT_ID,
                ORDERBY = menu.ORDERBY,
                PATH = menu.PATH,
                PERMISSION_ID = menu.PERMISSION_ID
            };

           
            return GenericResult<MenuResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<MenuResponseModel>> GetMenuAsync()
        {
            try
            {
                var menu = await _dbContext.Menus.Select(s => new MenuResponseModel

                {
                    MENU_ID = s.MENU_ID,
                    MENU_NAME = s.MENU_NAME,
                    MENU_DESCRIPTION = s.MENU_DESCRIPTION,
                    MENU_PARENT_ID = s.MENU_PARENT_ID,
                    ISACTIVED = s.ISACTIVE,
                    ORDERBY = s.ORDERBY,
                    PERMISSION_ID = s.PERMISSION_ID,
                    PATH = s.PATH,
                    Guid = s.GUID,
                    CREATEDDATE = s.CREATEDDATE,
                    CREATEDUSER = s.CREATEDUSER,
                    UPDATEDDATE = s.UPDATEDDATE,
                    UPDATEDUSER = s.UPDATEDUSER
                }).ToListAsync();
                return GenericResult<MenuResponseModel>.Succeed(menu);
            } catch (Exception ex) { throw; }

            return GenericResult<MenuResponseModel>.Succeed(new MenuResponseModel());
        }

        public async Task<GenericResult<bool>> UpdateMenuAsync(MenuRequestModel model)
        {
            if (model == null) return GenericResult<bool>.Fail(SystemMessageConstant.RequestInvalid);

            var menu = await _dbContext.Menus.FirstOrDefaultAsync(s => s.MENU_ID == model.MenuId);
            if (menu == null) return GenericResult<bool>.Fail(SystemMessageConstant.RequestInvalid);

            menu.MENU_NAME = model.MenuName;
            menu.MENU_DESCRIPTION = model.MenuDescription;
            menu.ISACTIVE = model.IsActived;
            menu.MENU_PARENT_ID = model.MenuParentId;
            menu.ORDERBY = model.OrderBy;
            menu.PATH = model.Path;
            menu.PERMISSION_ID = model.PermissionId;
            menu.UPDATEDDATE = DateTime.Now;
            menu.UPDATEDUSER = model.UserId;

            _dbContext.Menus.Update(menu);
            await _dbContext.SaveChangesAsync();
            return GenericResult<bool>.Succeeded;
        }
    }
}