﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebCore.Constants;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Models;
using WebCore.Models.Table;
using WebCore.Services.Interfaces;
using WebCore.ViewModels.Permission;

namespace WebCore.Services.Implementations
{
    public class PermissionService : IPermissionService
    {
        private readonly DwhContext _dbContext;

        public PermissionService(DwhContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<GenericResult<bool>> CreatePermissionAsync(PermissionRequestModel model)
        {
            if (model == null) return GenericResult<bool>.Failed;

            var permission = new Permission
            {
                CODE = model.PermissionCode,
                DESCRIPTION = model.Description,
                DISPLAYORDER = model.DisplayOrder,
                NAME = model.PermissionName,
                PARENTID = model.ParentId
            };

            _dbContext.Permissions.Add(permission);
            await _dbContext.SaveChangesAsync();

            return GenericResult<bool>.Succeeded;
        }

        public async Task<GenericResult<bool>> UpdatePermissionAsync(PermissionRequestModel model)
        {
            if (model == null) return GenericResult<bool>.Failed;

            var permission = await _dbContext.Permissions.FirstOrDefaultAsync(s => s.ID == model.Id);
            if (permission == null) return GenericResult<bool>.Fail(SystemMessageConstant.RequestInvalid);

            permission.NAME = model.PermissionName;
            //permission.DISPLAYORDER = model.DisplayOrder;
            //permission.PARENTID = model.ParentId;
            permission.ISACTIVE = model.IsActive;
            await _dbContext.SaveChangesAsync();

            return GenericResult<bool>.Succeeded;
        }

        public async Task<GenericResult<PermissionResponseModel>> GetAllPermissionAsync()
        {
            var permission = await _dbContext.Permissions.Select(s => new PermissionResponseModel
            {
                Code = s.CODE,
                Description = s.DESCRIPTION,
                DisplayOrder = s.DISPLAYORDER,
                Id = s.ID,
                Name = s.NAME,
            }).ToListAsync();

            return GenericResult<PermissionResponseModel>.Succeed(permission);
        }

        public async Task<GenericResult<PermissionResponseModel>> GetDetailPermissionAsync(long id)
        {
            if (id == 0) return GenericResult<PermissionResponseModel>.Failed;
            var permission = await _dbContext.Permissions.FirstOrDefaultAsync(s => s.ID == id);
            if (permission == null) return GenericResult<PermissionResponseModel>.Failed;

            var result = new PermissionResponseModel
            {
                Id = permission.ID, 
                Code = permission.CODE,
                Name = permission.NAME,
                Description = permission.DESCRIPTION,
                IsEnabled = permission.ISACTIVE == 1? true : false
            };


            return GenericResult<PermissionResponseModel>.Succeed(result);
        }

       
    }
}