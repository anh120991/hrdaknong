﻿using System;
using System.Data;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using WebCore.Constants;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Infrastructure.Extension;
using WebCore.Models.StoreExtension;
using WebCore.Services.Interfaces;
using WebCore.ViewModels;


namespace WebCore.Services.Implementations
{
    public class ReportService : IReportService
    {
        public async Task<GenericResult<DVKTKLResponseModel>> Donvi_ktkl()
        {
            var parameters = new OracleParameter[1];
            parameters[0] = new OracleParameter("v_out", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };
            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_DONVI_KT_KL, parameters);
            var result = dt.ConvertToList<DVKTKLResponseModel>();
            return GenericResult<DVKTKLResponseModel>.Succeed(result);
        }



        public async Task<GenericResult<TuoiResponseModel>> Dotuoi()
        {
            var parameters = new OracleParameter[1];
            parameters[0] = new OracleParameter("v_out", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };
            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_DOTUOI, parameters);
            var result = dt.ConvertToList<TuoiResponseModel>();
            return GenericResult<TuoiResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<DSKTKLResponseModel>> DS_khenthuong_kyluat(DVKTKLRequestModel model)
        {
            var parameters = new OracleParameter[6];
            int i = 0;
            parameters[i++] = new OracleParameter("V_FRM_DATE", model.V_FRM_DATE);
            parameters[i++] = new OracleParameter("V_TO_DATE", model.V_TO_DATE);
            parameters[i++] = new OracleParameter("V_SOHIEUCBCCVC", model.V_SOHIEUCBCCVC);
            parameters[i++] = new OracleParameter("V_DONVI", model.V_DONVI);
            parameters[i++] = new OracleParameter("V_LOAI", model.V_LOAI);
            parameters[i++] = new OracleParameter("v_out", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };
            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_DS_KHENTHUONG_KYLUAT, parameters);
            var result = dt.ConvertToList<DSKTKLResponseModel>();
            return GenericResult<DSKTKLResponseModel>.Succeed(result);
        }



        //        //var dt7 = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_DONVI_KT_KL, parameters8);

        //        //var dt11 = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_DM_DONVI, parameters12);
        //        //var dt12 = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_DM_CHUCVU, parameters13);
        //        //var dt13 = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_DM_TRANGTHAIHOSO, parameters14);
        //        //var dt14 = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_DS_HOSO, parameters15);

        //        var result = dt.ConvertToList<TotalSurGroupResponseModel>();
        //        return GenericResult<TotalSurGroupResponseModel>.Succeed(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        string s;
        //        throw;
        //    }


        //}

        public async Task<GenericResult<DangvienResponseModel>> Thongkedangvien()
        {
            var parameters = new OracleParameter[1];
            parameters[0] = new OracleParameter("v_out", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };
            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_THONGKEDANGVIEN, parameters);
            var result = dt.ConvertToList<DangvienResponseModel>();
            return GenericResult<DangvienResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<TKNhansuResponseModel>> Thongkenhansu()
        {
            var parameters = new OracleParameter[1];
            parameters[0] = new OracleParameter("v_out", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };
            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_THONGKENHANSU, parameters);
            var result = dt.ConvertToList<TKNhansuResponseModel>();
            return GenericResult<TKNhansuResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<NhansuResponseModel>> Thongkenhansu_Donvi()
        {
            var parameters = new OracleParameter[1];
            parameters[0] = new OracleParameter("v_out", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };
            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_THONGKENHANSU_DONVI, parameters);
            var result = dt.ConvertToList<NhansuResponseModel>();
            return GenericResult<NhansuResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<TileResponseModel>> Thongketylekhenthuong()
        {
            var parameters = new OracleParameter[1];
            parameters[0] = new OracleParameter("v_out", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };
            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_THONGKETYLEKHENTHUONG, parameters);
            var result = dt.ConvertToList<TileResponseModel>();
            return GenericResult<TileResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<TileResponseModel>> Thongketylekyluat()
        {
            var parameters = new OracleParameter[1];
            parameters[0] = new OracleParameter("v_out", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };
            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_THONGKETYLEKYLUAT, parameters);
            var result = dt.ConvertToList<TileResponseModel>();
            return GenericResult<TileResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<CBNSResponseModel>> Thongke_cbns()
        {
            var parameters = new OracleParameter[1];
            parameters[0] = new OracleParameter("v_out", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };
            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_THONGKE_CBNS, parameters);
            var result = dt.ConvertToList<CBNSResponseModel>();
            return GenericResult<CBNSResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<TDHVResponseModel>> Thongke_tdhv()
        {
            var parameters = new OracleParameter[1];
            parameters[0] = new OracleParameter("v_out", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };
            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_THONGKE_TDHV, parameters);
            var result = dt.ConvertToList<TDHVResponseModel>();
            return GenericResult<TDHVResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<TDNNResponseModel>> Thongke_tdnn()
        {
            var parameters = new OracleParameter[1];
            parameters[0] = new OracleParameter("v_out", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };
            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_THONGKE_TDNN, parameters);
            var result = dt.ConvertToList<TDNNResponseModel>();
            return GenericResult<TDNNResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<TinhocResponseModel>> Trinhdotinhoc()
        {
            var parameters = new OracleParameter[1];
            parameters[0] = new OracleParameter("v_out", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };
            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_TRINHDOTINHOC, parameters);
            var result = dt.ConvertToList<TinhocResponseModel>();
            return GenericResult<TinhocResponseModel>.Succeed(result);
        }
    }
}