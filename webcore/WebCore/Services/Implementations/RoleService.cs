﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebCore.Constants;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Models;
using WebCore.Models.Table;
using WebCore.Services.Interfaces;
using WebCore.ViewModels.Permission;
using WebCore.ViewModels.Role;

namespace WebCore.Services.Implementations
{
    public class RoleService : IRoleService
    {
        private readonly DwhContext _dbContext;

        public RoleService(DwhContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<GenericResult<bool>> CreateNewRoleAsync(RoleRequestModel model)
        {
            if (model == null) return GenericResult<bool>.Fail(SystemMessageConstant.RequestInvalid);

            var role = new Role
            {
                NAME = model.Name,
                DESCRIPTION = model.Description,
                CREATEDBY = 1,
                CREATEDDATETIME = DateTime.UtcNow,
                GUID = Guid.NewGuid().ToString()
            };

            if (model.PermissionIds != null && model.PermissionIds.Any())
            {
                role.RolePermissions = model.PermissionIds.Select(rolePermission => new RolePermissions
                {
                    PERMISSIONID = rolePermission.PermissionId,
                    ENABLED = rolePermission.IsEnabled ? 1 : 0
                }).ToList();
            }

            _dbContext.Add(role);
            await _dbContext.SaveChangesAsync();
            return GenericResult<bool>.Succeeded;
        }

        public async Task<GenericResult<bool>> UpdateRoleAsync(RoleRequestModel model)
        {
            if (model == null) return GenericResult<bool>.Fail(SystemMessageConstant.RequestInvalid);

            var role = await _dbContext.Roles.Include(s => s.RolePermissions).FirstOrDefaultAsync(s => s.ID == model.Id);
            if (role == null) return GenericResult<bool>.Fail(SystemMessageConstant.RequestInvalid);

            role.NAME = model.Name;
            role.DESCRIPTION = model.Description;

            if (model.PermissionIds != null && model.PermissionIds.Any())
            {
                role.RolePermissions = role.RolePermissions.Select(rolePermission =>
                {
                    rolePermission.ENABLED = 0;
                    return rolePermission;
                }).ToList();

                foreach (var item in model.PermissionIds)
                {
                    var rolePermission = role.RolePermissions.FirstOrDefault(s => s.PERMISSIONID == item.PermissionId);
                    if (rolePermission != null)
                        rolePermission.ENABLED = 1;
                    else
                    {
                        role.RolePermissions.Add(new RolePermissions
                        {
                            PERMISSIONID = item.PermissionId,
                            ROLEID = role.ID,
                            ENABLED = 1
                        });
                    }
                }
            }

            _dbContext.Roles.Update(role);
            await _dbContext.SaveChangesAsync();
            return GenericResult<bool>.Succeeded;
        }

        public async Task<GenericResult<RoleResponseModel>> GetAllRoleAsync()
        {
            var role = await _dbContext.Roles.Select(s => new RoleResponseModel
            {
                Description = s.DESCRIPTION,
                Id = s.ID,
                Guid = s.GUID,
                Name = s.NAME,
                CreatedBy = s.CREATEDBY != null ? int.Parse(s.CREATEDBY.ToString()) : 0,
                CreatedDate = s.CREATEDDATETIME 
            }).ToListAsync();

            return GenericResult<RoleResponseModel>.Succeed(role);
        }

        public async Task<GenericResult<RoleResponseModel>> GetDetailRoleAsync(long id)
        {
            if (id == 0) return GenericResult<RoleResponseModel>.Failed;
            var role = await _dbContext.Roles.Include(s => s.RolePermissions).ThenInclude(s => s.Permission).FirstOrDefaultAsync(s => s.ID == id);
            if (role == null) return GenericResult<RoleResponseModel>.Failed;

            var result = new RoleResponseModel
            {
                Name = role.NAME,
                Description = role.DESCRIPTION,
                Id = role.ID,
                Guid = role.GUID,
            };

            if (role.RolePermissions != null && role.RolePermissions.Any())
            {
                result.Permissions = role.RolePermissions.Where(s => s.ENABLED == 1).Select(s => new PermissionResponseModel
                {
                    Code = s.Permission.CODE,
                    Name = s.Permission.NAME,
                    Id = s.PERMISSIONID,
                    IsEnabled = s.ENABLED == 1
                }).ToList();
            }

            return GenericResult<RoleResponseModel>.Succeed(result);
        }
    }
}
