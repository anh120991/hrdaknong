﻿using System.Data;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using WebCore.Constants;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Infrastructure.Extension;
using WebCore.Models.StoreExtension;
using WebCore.Services.Interfaces;
using WebCore.ViewModels.Treatment;

namespace WebCore.Services.Implementations
{
    public class TreatmentService : ITreatmentService
    {
        public async Task<GenericResult<TotalIpPatientsResponseModel>> GetTotalIpPatientsAsync(TreatmentRequestModel model)
        {
            var parameters = new OracleParameter[3];
            int i = 0;
            parameters[i++] = new OracleParameter("P_IN_FRM_DATE", model.FromDate?.ToString("dd/MM/yyyy"));
            parameters[i++] = new OracleParameter("P_IN_TO_DATE ", model.ToDate?.ToString("dd/MM/yyyy"));
            parameters[i++] = new OracleParameter("P_OUT_RESULT ", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };

            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_TOTAL_IP_P, parameters);

            var result = dt.ConvertToList<TotalIpPatientsResponseModel>();
            return GenericResult<TotalIpPatientsResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<TotalIpAdmitDeptResponseModel>> GetTotalIpAdmitPatientsAsync(TreatmentRequestModel model)
        {
            var parameters = new OracleParameter[3];
            int i = 0;
            parameters[i++] = new OracleParameter("P_IN_FRM_DATE", model.FromDate?.ToString("dd/MM/yyyy"));
            parameters[i++] = new OracleParameter("P_IN_TO_DATE ", model.ToDate?.ToString("dd/MM/yyyy"));
            parameters[i++] = new OracleParameter("P_OUT_RESULT ", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };

            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_TOTAL_IP_ADMIT_DEPT, parameters);

            var result = dt.ConvertToList<TotalIpAdmitDeptResponseModel>();
            return GenericResult<TotalIpAdmitDeptResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<TotalIpOutDeptResponseModel>> GetTotalIpOutDeptAsync(TreatmentRequestModel model)
        {
            var parameters = new OracleParameter[3];
            int i = 0;
            parameters[i++] = new OracleParameter("P_IN_FRM_DATE", model.FromDate?.ToString("dd/MM/yyyy"));
            parameters[i++] = new OracleParameter("P_IN_TO_DATE ", model.ToDate?.ToString("dd/MM/yyyy"));
            parameters[i++] = new OracleParameter("P_OUT_RESULT ", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };

            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_TOTAL_IP_OUT_DEPT, parameters);

            var result = dt.ConvertToList<TotalIpOutDeptResponseModel>();
            return GenericResult<TotalIpOutDeptResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<TotalIpTreatmentResponseModel>> GetTotalIpTreatmentAsync(TreatmentRequestModel model)
        {
            var parameters = new OracleParameter[3];
            int i = 0;
            parameters[i++] = new OracleParameter("P_IN_FRM_DATE", model.FromDate?.ToString("dd/MM/yyyy"));
            parameters[i++] = new OracleParameter("P_IN_TO_DATE ", model.ToDate?.ToString("dd/MM/yyyy"));
            parameters[i++] = new OracleParameter("P_OUT_RESULT ", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };

            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_TOTAL_IP_TREATMENT, parameters);

            var result = dt.ConvertToList<TotalIpTreatmentResponseModel>();
            return GenericResult<TotalIpTreatmentResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<TotalIpResultResponseModel>> GetTotalIpResultAsync(TreatmentRequestModel model)
        {
            var parameters = new OracleParameter[3];
            int i = 0;
            parameters[i++] = new OracleParameter("P_IN_FRM_DATE", model.FromDate?.ToString("dd/MM/yyyy"));
            parameters[i++] = new OracleParameter("P_IN_TO_DATE ", model.ToDate?.ToString("dd/MM/yyyy"));
            parameters[i++] = new OracleParameter("P_OUT_RESULT ", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };

            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_TOTAL_IP_RESULT, parameters);

            var result = dt.ConvertToList<TotalIpResultResponseModel>();
            return GenericResult<TotalIpResultResponseModel>.Succeed(result);
        }

        public async Task<GenericResult<TotalIpPatientSubjectResponseModel>> GetTotalIpPatientSubjectAsync(TreatmentRequestModel model)
        {
            var parameters = new OracleParameter[3];
            int i = 0;
            parameters[i++] = new OracleParameter("P_IN_FRM_DATE", model.FromDate?.ToString("dd/MM/yyyy"));
            parameters[i++] = new OracleParameter("P_IN_TO_DATE ", model.ToDate?.ToString("dd/MM/yyyy"));
            parameters[i++] = new OracleParameter("P_OUT_RESULT ", OracleDbType.RefCursor) { Direction = ParameterDirection.Output };

            var dt = await CallStoreOracleExtension.GetDataTable(StoreProcedureConstant.SP_TOTAL_IP_PATIENT_SUBJECT, parameters);

            var result = dt.ConvertToList<TotalIpPatientSubjectResponseModel>();
            return GenericResult<TotalIpPatientSubjectResponseModel>.Succeed(result);
        }
    }
}