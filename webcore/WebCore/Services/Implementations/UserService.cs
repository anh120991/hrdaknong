﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebCore.Constants;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.Infrastructure.Helper;
using WebCore.Models;
using WebCore.Models.Table;
using WebCore.Services.Interfaces;
using WebCore.ViewModels.User;

namespace WebCore.Services.Implementations
{
    public class UserService : IUserService
    {
        private readonly DwhContext _dwhContext;

        public UserService(DwhContext dwhContext)
        {
            _dwhContext = dwhContext;
        }

        public async Task<GenericResult<bool>> CreateUserAsync(UserRequestModel model)
        {
            if (model == null)
            {
                return GenericResult<bool>.Fail(SystemMessageConstant.RequestInvalid);
            }

            var userExists = await _dwhContext.Users.FirstOrDefaultAsync(s => s.USERNAME == model.Username);
            if (userExists != null)
            {
                return GenericResult<bool>.Fail("Tài khoản đã tồn tại");
            }

            var passwordHash = CryptoHelpers.SecureHash(model.Password, out var salt);
            var user = new User
            {
                USERNAME = model.Username,
                ISACTIVE = 1,
                PASSWORDHASH = passwordHash,
                SALT = salt,
                ISLOCKOUT = 0,
                DELETED = 0,
                CREATEDDATE = DateTime.Now
            };

            var profile = new Profile
            {
                FULLNAME = model.Fullname,
                EMAIL = model.Email,
                ADDRESS = model.Address,
                AVATAR = model.Avatar,
                GENDER = model.Gender,
                DEPARTMENTID = model.DepartmentId,
                USER = user
            };
            user.Profiles = new List<Profile> { profile };
            //Thêm vai trò
            
            if (model.RoleIds != null && model.RoleIds.Any())
            {
                user.UserRoles = model.RoleIds.Select(roleId => new UserRole
                {
                    ROLEID = roleId

                }).ToList();
            }
            //thêm phòng ban
            if (model.DepartmentIds != null && model.DepartmentIds.Any())
            {
                user.UserDepartments = model.DepartmentIds.Select(department => new UserDepartment
                {
                    DEPARTMENTID = department
                }).ToList();
            }

                //thêm nhóm người dùng
            if (model.GroupIds != null && model.GroupIds.Any())
            {
                user.UserGroups = model.GroupIds.Select(groupId => new UserGroup
                {
                    GROUPID = groupId
                }).ToList();
            }

            _dwhContext.Users.Add(user);
            await _dwhContext.SaveChangesAsync();

            return GenericResult<bool>.Succeeded;
        }

        public async Task<GenericResult<UserResponseModel>> GetAllUser()
        {
            var user = await (from u in _dwhContext.Users
                              join pr in _dwhContext.Profiles on u.ID equals pr.USERID
                              select new UserResponseModel
                              {
                                  Email = pr.EMAIL,
                                  Address = pr.ADDRESS,
                                  Fullname = pr.FULLNAME,
                                  IsActive = u.ISACTIVE == 1,
                                  Username = u.USERNAME,
                                  Id = u.ID
                              }).ToListAsync();

            return GenericResult<UserResponseModel>.Succeed(user);
        }

        public async Task<GenericResult<UserResponseModel>> GetDetailUser(long id)
        {
            try
            {
                var user = await (from u in _dwhContext.Users.Include(s => s.UserRoles).Include(s => s.UserDepartments).Include(s => s.UserGroups)
                                  join pr in _dwhContext.Profiles on u.ID equals pr.USERID
                                  where u.ID == id
                                  select new UserResponseModel
                                  {
                                      Email = pr.EMAIL,
                                      Address = pr.ADDRESS,
                                      Fullname = pr.FULLNAME,
                                      IsActive = u.ISACTIVE == 1,
                                      Username = u.USERNAME,
                                      Gender = pr.GENDER,
                                      DepartmentIds = u.UserDepartments != null && u.UserDepartments.Count > 0 ? u.UserDepartments.Select(s => s.DEPARTMENTID).ToList() : new List<long>(),
                                      RoleIds = u.UserRoles != null && u.UserRoles.Count > 0 ? u.UserRoles.Select(s => s.ROLEID).ToList() : new List<long>(),
                                      GroupIds = u.UserGroups != null && u.UserGroups.Count > 0 ? u.UserGroups.Select(s => s.GROUPID).ToList() : new List<int>(),
                                      Id = u.ID
                                  }).FirstOrDefaultAsync();



                return GenericResult<UserResponseModel>.Succeed(user);
            }
           catch(Exception ex)
            {
                throw;
            }

            
        }

        public async Task<GenericResult<bool>> UpdateUserAsync(UserRequestModel model)
        {
            if (model == null || model.Id == 0) return GenericResult<bool>.Failed;

            var user = await _dwhContext.Users.Include(s => s.UserRoles).Include(s => s.UserDepartments).FirstOrDefaultAsync(s => s.ID == model.Id);
            if (user == null) return GenericResult<bool>.Failed;

            user.ISLOCKOUT = model.IsLockOut ? 1 : 0;
            if (model.IsLockOut)
                user.LOCKOUTEND = model.LockOutEnd;
            user.MODIFIEDDATE = DateTime.Now;

            var profile = await _dwhContext.Profiles.FirstOrDefaultAsync(s => s.USERID == model.Id);
            if (profile != null)
            {
                profile.ADDRESS = model.Address;
                profile.AVATAR = model.Avatar;
                profile.EMAIL = model.Email;
                profile.FULLNAME = model.Fullname;
                profile.GENDER = model.Gender;
            }

            var userRole = user.UserRoles;
            if (userRole != null && userRole.Count > 0)
                userRole.Clear();

            if (model.RoleIds != null && model.RoleIds.Any())
            {
                userRole = model.RoleIds.Select(role => new UserRole
                {
                    USERID = model.Id,
                    ROLEID = role
                }).ToList();

                user.UserRoles = userRole;
            }

            var userDepartment = user.UserDepartments ?? new List<UserDepartment>();
            if (userDepartment != null && userDepartment.Count > 0)
                user.UserDepartments = new List<UserDepartment>();
            if (model.DepartmentIds != null && model.DepartmentIds.Any())
            {
                userDepartment = model.DepartmentIds.Select(departmentId => new UserDepartment
                {
                    DEPARTMENTID = departmentId,
                    USERID = user.ID
                }).ToList();

                user.UserDepartments = userDepartment;
            }


            var userGroup = user.UserGroups ?? new List<UserGroup>();
            if (userGroup != null && userGroup.Count > 0)
                user.UserGroups = new List<UserGroup>();
            if (model.GroupIds != null && model.GroupIds.Any())
            {
                userGroup = model.GroupIds.Select(userGroupId => new UserGroup
                {
                    GROUPID = userGroupId,
                    USERID = user.ID
                }).ToList();

                user.UserGroups = userGroup;
            }

            _dwhContext.Users.Update(user);
            await _dwhContext.SaveChangesAsync();
            return GenericResult<bool>.Succeeded;
        }

        public async Task<GenericResult<bool>> ChangePasswordAsync(UserChangePasswordRequestModel model)
        {
            if (model == null) return GenericResult<bool>.Failed;

            var user = await _dwhContext.Users.Include(s => s.UserRoles).FirstOrDefaultAsync(s => s.ID == model.Id);
            if (user == null) return GenericResult<bool>.Failed;

            var pwdHash = CryptoHelpers.SecureHash(model.OldPassword, user.SALT);

            if (pwdHash != user.PASSWORDHASH)
            {
                return GenericResult<bool>.Fail("Username or password incorrect");
            }

            var passwordHash = CryptoHelpers.SecureHash(model.NewPassword, out var salt);
            user.PASSWORDHASH = passwordHash;
            user.SALT = salt;

            user.MODIFIEDDATE = DateTime.Now;
            await _dwhContext.SaveChangesAsync();
            return GenericResult<bool>.Succeeded;
        }

        public async Task<GenericResult<UserGroupResponseModel>> GetAllUserGroup()
        {
            var userGroup = await(from u in _dwhContext.Groups
                             select new UserGroupResponseModel
                             {
                               GroupId = u.GROUPID,
                               GroupName = u.GROUPNAME,
                               IsMultilDepartments = u.ISMULTILDEPARTMENTS
                             }).ToListAsync();

            return GenericResult<UserGroupResponseModel>.Succeed(userGroup);
        }
    }
}