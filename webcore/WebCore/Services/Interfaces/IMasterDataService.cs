﻿using System.Threading.Tasks;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.ViewModels;
using WebCore.ViewModels.Treatment;


namespace WebCore.Services.Interfaces
{
    public interface IMasterDataService
    {
        Task<GenericResult<DMDVResponseModel>> DM_DonVi();
        Task<GenericResult<PositionResponseModel>> DM_Chucvu();
        Task<GenericResult<StatusResponseModel>> DM_Trangthaihoso();
        Task<GenericResult<HSResponseModel>> DS_HS(DSHSRequestModel model);
    }
}