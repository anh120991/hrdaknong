﻿using System.Threading.Tasks;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.ViewModels;
using WebCore.ViewModels.Chart;

using WebCore.ViewModels.Menu;


namespace WebCore.Services.Interfaces
{
    public interface IMenuService
    {
        Task<GenericResult<MenuResponseModel>> GetMenuAsync();
        Task<GenericResult<bool>> CreateNewMenuAsync(MenuRequestModel model);
        Task<GenericResult<bool>> UpdateMenuAsync(MenuRequestModel model);
        Task<GenericResult<MenuResponseModel>> GetDetailMenuAsync(long id);
    }
}