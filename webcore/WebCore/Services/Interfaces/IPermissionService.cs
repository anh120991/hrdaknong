﻿using System.Threading.Tasks;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.ViewModels.Permission;

namespace WebCore.Services.Interfaces
{
    public interface IPermissionService
    {
        Task<GenericResult<bool>> CreatePermissionAsync(PermissionRequestModel model);
        Task<GenericResult<bool>> UpdatePermissionAsync(PermissionRequestModel model);
        Task<GenericResult<PermissionResponseModel>> GetAllPermissionAsync();
        Task<GenericResult<PermissionResponseModel>> GetDetailPermissionAsync(long id);
        
    }
}