﻿using System.Threading.Tasks;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.ViewModels;

namespace WebCore.Services.Interfaces
{
    public interface IReportService
    {
        Task<GenericResult<TKNhansuResponseModel>> Thongkenhansu();
        Task<GenericResult<DangvienResponseModel>> Thongkedangvien();
        Task<GenericResult<TuoiResponseModel>> Dotuoi();
        Task<GenericResult<TinhocResponseModel>> Trinhdotinhoc();
        Task<GenericResult<NhansuResponseModel>> Thongkenhansu_Donvi();
        Task<GenericResult<TileResponseModel>> Thongketylekhenthuong();
        Task<GenericResult<TileResponseModel>> Thongketylekyluat();
        Task<GenericResult<DVKTKLResponseModel>> Donvi_ktkl();
        Task<GenericResult<TDHVResponseModel>> Thongke_tdhv();
        Task<GenericResult<TDNNResponseModel>> Thongke_tdnn();
        Task<GenericResult<CBNSResponseModel>> Thongke_cbns();
        //
        Task<GenericResult<DSKTKLResponseModel>> DS_khenthuong_kyluat(DVKTKLRequestModel model);
    }
}