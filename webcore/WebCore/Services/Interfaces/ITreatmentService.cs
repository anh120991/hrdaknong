﻿using System.Threading.Tasks;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.ViewModels.Treatment;

namespace WebCore.Services.Interfaces
{
    public interface ITreatmentService
    {
        Task<GenericResult<TotalIpPatientsResponseModel>> GetTotalIpPatientsAsync(TreatmentRequestModel model);
        Task<GenericResult<TotalIpAdmitDeptResponseModel>> GetTotalIpAdmitPatientsAsync(TreatmentRequestModel model);
        Task<GenericResult<TotalIpOutDeptResponseModel>> GetTotalIpOutDeptAsync(TreatmentRequestModel model);
        Task<GenericResult<TotalIpTreatmentResponseModel>> GetTotalIpTreatmentAsync(TreatmentRequestModel model);
        Task<GenericResult<TotalIpResultResponseModel>> GetTotalIpResultAsync(TreatmentRequestModel model);
        Task<GenericResult<TotalIpPatientSubjectResponseModel>> GetTotalIpPatientSubjectAsync(TreatmentRequestModel model);
    }
}