﻿using System.Threading.Tasks;
using WebCore.Infrastructure.BusinessObjects;
using WebCore.ViewModels.User;

namespace WebCore.Services.Interfaces
{
    public interface IUserService
    {
        Task<GenericResult<bool>> CreateUserAsync(UserRequestModel model);
        Task<GenericResult<UserResponseModel>> GetAllUser();
        Task<GenericResult<UserResponseModel>> GetDetailUser(long id);
        Task<GenericResult<bool>> UpdateUserAsync(UserRequestModel model);
        Task<GenericResult<bool>> ChangePasswordAsync(UserChangePasswordRequestModel model);

        Task<GenericResult<UserGroupResponseModel>> GetAllUserGroup();
    }
}