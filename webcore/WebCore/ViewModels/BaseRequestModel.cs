﻿using System;

namespace WebCore.ViewModels
{
    public class BaseRequestModel
    {
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}