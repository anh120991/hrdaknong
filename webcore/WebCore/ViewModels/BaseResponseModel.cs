﻿using System;

namespace WebCore.ViewModels
{
    public class BaseResponseModel
    {
        public DateTime SUR_DATE { get; set; }
        public int SUR_PATIENTS { get; set; }
        public decimal SUR_QTY { get; set; }
    }
}