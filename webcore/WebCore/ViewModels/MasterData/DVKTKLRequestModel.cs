﻿using System;

namespace WebCore.ViewModels
{
    public class DSHSRequestModel
    {
        public DateTime? fromDate  { get; set; }
        public DateTime? toDate { get; set; }
        public int? donVi { get; set; }
         public int? chucVu { get; set; }
        public int? trangThai { get; set; }
    }
}