﻿namespace WebCore.ViewModels
{
    public class HSResponseModel
    {
        public string HOVATEN { get; set; }
        public string NGAYSINH { get; set; }
        public int SOHIEUCBCCVC { get; set; }
        public string GIOITINH { get; set; }
        public string TENTRANGTHAI { get; set; }
        public string TENDONVI { get; set; }
        public string TENCHUCVU { get; set; }

    }
}