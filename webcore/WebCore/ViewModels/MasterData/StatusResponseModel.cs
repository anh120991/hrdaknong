﻿namespace WebCore.ViewModels
{
    public class DMDVResponseModel
    {
        public int MADONVI { get; set; }
        public string TENDONVI { get; set; }
        public int LOAI { get; set; }
        public string MA { get; set; }
        public int TRENCAP { get; set; }
    }
}