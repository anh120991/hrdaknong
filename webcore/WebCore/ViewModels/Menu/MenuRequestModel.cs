﻿using System.Collections.Generic;

namespace WebCore.ViewModels.Menu
{
    public class MenuRequestModel
    {
        public long MenuId { get; set; }
        public string MenuName { get; set; }
        public long MenuParentId { get; set; }
        public int IsActived { get; set; }
        public string MenuDescription { get; set; }
        public int OrderBy { get; set; }
        public int PermissionId { get; set; }
        public string Path { get; set; }
        public string UserId { get; set; }
    }
}