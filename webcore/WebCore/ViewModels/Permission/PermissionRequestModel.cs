﻿namespace WebCore.ViewModels.Permission
{
    public class PermissionRequestModel: BaseModel
    {
        public string PermissionName { get; set; }
        public string PermissionCode { get; set; }
        public long ParentId { get; set; }
        public string Description { get; set; }
        public long DisplayOrder { get; set; }
        public int IsActive { get; set; }
    }
}