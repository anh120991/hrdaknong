﻿namespace WebCore.ViewModels
{
    public class DSKTKLResponseModel
    {
        public string HOVATEN { get; set; }
        public int SOHIEU { get; set; }
        public string DONVI { get; set; }
        public string HINHTHUC { get; set; }
        public string NGAY { get; set; }
    }
}