﻿using System;

namespace WebCore.ViewModels
{
    public class DVKTKLRequestModel
    {
        public DateTime? V_FRM_DATE { get; set; }
        public DateTime? V_TO_DATE { get; set; }
        public int? V_SOHIEUCBCCVC { get; set; }
         public int V_DONVI { get; set; }
        public int? V_LOAI { get; set; }
    }
}