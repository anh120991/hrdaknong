﻿namespace WebCore.ViewModels
{
    public class DVKTKLResponseModel
    {
        public int MADONVI { get; set; }
        public string TENDONVI { get; set; }
        public int KHENTHUONG { get; set; }
        public int KYLUAT { get; set; }
    }
}