﻿namespace WebCore.ViewModels
{
    public class TuoiResponseModel
    {
        public int NAM_DOTUOI1 { get; set; }
        public int NAM_DOTUOI2 { get; set; }
        public int NAM_DOTUOI3 { get; set; }
        public int NAM_DOTUOI4 { get; set; }
        public int NAM_DOTUOI5 { get; set; }
        public int NAM_DOTUOI6 { get; set; }
        public int NAM_DOTUOI7 { get; set; }
        public int NU_DOTUOI1 { get; set; }
        public int NU_DOTUOI2 { get; set; }
        public int NU_DOTUOI3 { get; set; }
        public int NU_DOTUOI4 { get; set; }
        public int NU_DOTUOI5 { get; set; }
        public int NU_DOTUOI6 { get; set; }
        public int NU_DOTUOI7 { get; set; }

    }
}