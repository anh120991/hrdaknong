﻿using System;

namespace WebCore.ViewModels.Treatment
{
    public class IpPatientsResponseBaseModel
    {
        public DateTime IP_DATE { get; set; }
        public int IP_PATIENTS { get; set; }
    }
}