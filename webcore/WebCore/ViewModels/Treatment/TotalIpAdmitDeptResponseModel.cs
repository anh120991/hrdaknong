﻿namespace WebCore.ViewModels.Treatment
{
    public class TotalIpAdmitDeptResponseModel : IpPatientsResponseBaseModel
    {
        public int IP_DEPT_ID { get; set; }
    }
}