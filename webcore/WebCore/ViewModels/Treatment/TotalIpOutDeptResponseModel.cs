﻿namespace WebCore.ViewModels.Treatment
{
    public class TotalIpOutDeptResponseModel : IpPatientsResponseBaseModel
    {
        public int IP_DEPT_ID { get; set; }
    }
}