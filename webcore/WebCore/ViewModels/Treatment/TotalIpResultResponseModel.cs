﻿namespace WebCore.ViewModels.Treatment
{
    public class TotalIpResultResponseModel : IpPatientsResponseBaseModel
    {
        public int IP_RES_ID { get; set; }
    }
}