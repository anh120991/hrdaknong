﻿namespace WebCore.ViewModels.Treatment
{
    public class TotalIpTreatmentResponseModel : IpPatientsResponseBaseModel
    {
        public int IP_TM_ID { get; set; }
    }
}