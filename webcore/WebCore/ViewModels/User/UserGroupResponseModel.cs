﻿using System.Collections.Generic;

namespace WebCore.ViewModels.User
{
    public class UserGroupResponseModel
    {
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public int IsMultilDepartments { get; set; }

    }
}