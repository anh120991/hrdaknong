﻿using System;
using System.Collections.Generic;

namespace WebCore.ViewModels.User
{
    public class UserRequestModel
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Avatar { get; set; }
        public int DepartmentId { get; set; }
        public int Gender { get; set; }

        public bool IsActive { get; set; }
        public bool IsLockOut { get; set; }
        public DateTime LockOutEnd { get; set; }

        public List<int> RoleIds { get; set; }
        public List<int> DepartmentIds { get; set; }
        public List<int> GroupIds { get; set; }
    }
}
